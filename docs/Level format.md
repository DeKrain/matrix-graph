# Level format

## Version 1
File format is a JSON document with structure as follows:
- `version`: `u32` - identifies the version of the format; currently it is `1`
- `player` - info about the player
	- `position`: `[3]f32` - position of the player
	- `velocity`: `[3]f32` - velocity of the player
	- `state`: `enum` - state the player is currently in
		- `"normal"` - player moves normally
		- `"flying"` - player is in free-flight mode
- `camera` [_optional_] - info about the camera; may be missing
	- `yaw`: `f32` - yaw/horizontal angle made from `-Z` direction increasing into `+X` direction; wrapped between `-τ/2` and `τ/2`
	- `pitch`: `f32` - pitch/vertical angle; straight forward is `0`, up is positive, down is negative; clamped between `-τ/4` and `τ/4`
- `parts`: `list` - list of parts present in the level; layout is work in progress
	- (_element_)
		- `origin`: `[3]f32` - origin of the part
		- `extent`: `[3]f32` - extent from the origin; _should_ be positive
		- `interaction`: `enum` - unfinished; specifies the interaction type of the part
			- `"static"` - this part is _part_ of static geometry
			- `"program"` - unused; a part that is programmable by players
