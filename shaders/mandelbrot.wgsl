// SPDX-License-Identifier: Apache-2.0
// Copyright © DeKrain circa 2018
// Copyright © DeKrain 2023

// Adapted to WGSL from WebGLSL

// enable f16;
alias flow = f32;
alias vec3flow = vec3<f32>;

struct VertexOutput {
	@builtin(position) clip: vec4<f32>,
	@location(0) coord: vec2<f32>,
}

@vertex
fn vs_main(@builtin(vertex_index) index: u32) -> VertexOutput {
	var out: VertexOutput;
	var vertices: array<vec2<f32>, 4> = array(
		vec2( 1.0, -1.0),
		vec2( 1.0,  1.0),
		vec2(-1.0, -1.0),
		vec2(-1.0,  1.0),
	);

	let vert = vertices[index];
	out.clip = vec4(vert, 0.0, 1.0);
	out.coord = (vert + 1.0) * 0.5;
	return out;
}

// x: 0 - 1 -> -2.5 - 1
// S1. x = x * 3.5 // * dist(-2.5, 1)
// S2. x = x - 2.5

const w: f32 = 875.0;
const h: f32 = 500.0;

const colors: array<vec3flow, 14> = array(
	vec3(0.0, 0.0, 0.0),
	vec3(0.0, 0.0, 0.5),
	vec3(0.0, 0.0, 1.0),
	vec3(0.0, 0.5, 1.0),
	vec3(0.0, 1.0, 1.0),
	vec3(0.0, 1.0, 0.5),
	vec3(0.0, 1.0, 0.0),
	vec3(0.5, 1.0, 0.0),
	vec3(1.0, 1.0, 0.0),
	vec3(1.0, 0.5, 0.0),
	vec3(1.0, 0.0, 0.0),
	vec3(1.0, 0.0, 0.5),
	vec3(1.0, 0.0, 1.0),
	vec3(0.5, 0.0, 0.5),
);

const max_iter: u32 = 1000u;

@fragment
fn fs_main(@location(0) coord: vec2<f32>) -> @location(0) vec4<f32> {
	let ox = (coord.x * 3.5) - 2.5;
	let oy = (coord.y * 2.0) - 1.0;
	var x: f32 = 0.0;
	var y: f32 = 0.0;
	var xtemp: f32;
	var iter: u32 = 0u;
	for (var f_iter: u32 = 0u; f_iter < max_iter; f_iter += 1u) {
		xtemp = ((x*x) - (y*y)) + ox;
		y = (2.0*x*y) + oy;
		x = xtemp;
		iter += 1u;
		if (x*x + y*y) >= 4.0 {
			break;
		}
	}
	//var idx: u32;
	var out: vec3<f32>;
	if iter == max_iter { out = colors[0]; }
	else if iter >= 60u { out = colors[1]; }
	else if iter >= 55u { out = colors[2]; }
	else if iter >= 50u { out = colors[3]; }
	else if iter >= 45u { out = colors[4]; }
	else if iter >= 40u { out = colors[5]; }
	else if iter >= 35u { out = colors[6]; }
	else if iter >= 30u { out = colors[7]; }
	else if iter >= 25u { out = colors[8]; }
	else if iter >= 20u { out = colors[9]; }
	else if iter >= 15u { out = colors[10]; }
	else if iter >= 10u { out = colors[11]; }
	else if iter >= 5u { out = colors[12]; }
	else { out = colors[13]; }
	//return vec4<f32>(colors[idx], 1.0);
	return vec4<f32>(out, 1.0);
}
