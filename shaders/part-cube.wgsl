// Global attributes
@group(0) @binding(0) var<storage, read> vp_transform: mat4x4<f32>;

struct VertexInput {
	// Instanced attributes
	@location(0) origin: vec3<f32>,
	@location(1) extent: vec3<f32>,
	// Vertex attributes
	@builtin(vertex_index) index: u32,
}

struct VertexOutput {
	@builtin(position) position: vec4<f32>,
}

@vertex
fn vs_main(input: VertexInput) -> VertexOutput {
	// World position
	let base = vec3(
		f32((input.index & 1u) != 0u),
		f32((input.index & 2u) != 0u),
		f32((input.index & 4u) != 0u),
	) * input.extent + input.origin;

	// Clip position
	var out: VertexOutput;
	out.position = vp_transform * vec4(base, 1.0);
	return out;
}

@fragment
fn fs_fill() -> @location(0) vec4<f32> {
	return vec4(1.0, 1.0, 1.0, 1.0);
}

@fragment
fn fs_outline() -> @location(0) vec4<f32> {
	return vec4(0.0, 0.0, 0.0, 1.0);
}
