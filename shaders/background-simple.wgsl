struct VertexOutput {
	@builtin(position) clip: vec4<f32>,
	@location(0) coord: vec2<f32>,
}

@vertex
fn vs_main(@builtin(vertex_index) index: u32) -> VertexOutput {
	var out: VertexOutput;
	var vertices: array<vec2<f32>, 4> = array(
		vec2( 1.0, -1.0),
		vec2( 1.0,  1.0),
		vec2(-1.0, -1.0),
		vec2(-1.0,  1.0),
	);

	let vert = vertices[index];
	out.clip = vec4(vert, 0.0, 1.0);
	out.coord = (vert + 1.0) * 0.5;
	return out;
}

const light = vec3(0.5, 0.5, 0.5);
const dark  = vec3(0.2, 0.2, 0.2);
const high = 0.5;
const low = 0.2;
const band = high - low;
const band_rev = 1.0 / band;

@fragment
fn fs_main(@location(0) coord: vec2<f32>) -> @location(0) vec4<f32> {
	if coord.y >= high {
		return vec4(light, 1.0);
	}
	if coord.y <= low {
		return vec4(dark, 1.0);
	}
	return vec4(mix(dark, light, (coord.y - low) * band_rev), 1.0);
}
