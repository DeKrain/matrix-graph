// Global attributes
@group(0) @binding(0) var<storage, read> vp_transform: mat4x4<f32>;

// Ghost attributes
struct Attributes {
	origin: vec3<f32>,
	extent: vec3<f32>,
	color: u32,
}
@group(1) @binding(0) var<storage, read> attributes: Attributes;

struct VertexOutput {
	@builtin(position) position: vec4<f32>,
}

@vertex
fn vs_main(@builtin(vertex_index) index: u32) -> VertexOutput {
	// World position
	let base = vec3(
		f32((index & 1u) != 0u),
		f32((index & 2u) != 0u),
		f32((index & 4u) != 0u),
	) * attributes.extent + attributes.origin;

	// Clip position
	var out: VertexOutput;
	out.position = vp_transform * vec4(base, 1.0);
	return out;
}

@fragment
fn fs_main() -> @location(0) vec4<f32> {
	let color = unpack4x8unorm(attributes.color);
	return color;
}
