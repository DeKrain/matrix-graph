// Global attributes
@group(0) @binding(0) var<storage, read> vp_transform: mat4x4<f32>;

struct VertexInput {
	// Instanced attributes
	@location(0) origin: vec3<f32>,
	@location(1) extent: vec3<f32>,
	@location(2) color: vec4<f32>,
	// Vertex attributes
	@builtin(vertex_index) index: u32,
}

struct VertexOutput {
	@builtin(position) position: vec4<f32>,
	@location(0) @interpolate(flat) color: vec4<f32>,
}

const WIRE_OFFSET = 0.125;

fn vs_main_world(input: VertexInput) -> VertexOutput {
	// World position
	let offset = select(WIRE_OFFSET, -WIRE_OFFSET, (input.index & 8u) == 0u);
	let base = vec3(
		select(-offset, input.extent.x + offset, (input.index & 1u) != 0u),
		select(-offset, input.extent.y + offset, (input.index & 2u) != 0u),
		select(-offset, input.extent.z + offset, (input.index & 4u) != 0u),
	) + input.origin;

	// Clip position
	var out: VertexOutput;
	out.position = vp_transform * vec4(base, 1.0);
	out.color = input.color;
	return out;
}

fn vs_main_viewport(input: VertexInput) -> VertexOutput {
	// World position
	let offset = select(WIRE_OFFSET, -WIRE_OFFSET, (input.index & 8u) == 0u);
	let base = vec3(
		f32((input.index & 1u) != 0u),
		f32((input.index & 2u) != 0u),
		f32((input.index & 4u) != 0u),
	) * input.extent + input.origin;
	let offset2d = vec2(
		select(-offset, offset, (input.index & 1u) != 0u),
		select(-offset, offset, (input.index & 2u) != 0u),
	);

	// Clip position
	var out: VertexOutput;
	out.position = vp_transform * vec4(base, 1.0);
	out.position.x += offset2d.x;
	out.position.y += offset2d.y;
	out.color = input.color;
	return out;
}

@vertex
fn vs_main(input: VertexInput) -> VertexOutput {
	return vs_main_world(input);
}

@fragment
fn fs_outline(
	@location(0) @interpolate(flat) color: vec4<f32>,
) -> @location(0) vec4<f32> {
	return color;
}
