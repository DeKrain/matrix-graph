struct VertexInput {
	@builtin(vertex_index) index: u32,

	// Stored as one type in UIRect
	// Position & extent are provided in [0..size]
	@location(0) position: vec2<f32>,
	@location(1) extent: vec2<f32>,

	@location(2) color: vec4<f32>,
	@location(3) round_corners: vec4<f32>,
}

struct VertexOutput {
	@builtin(position) position: vec4<f32>,
	@location(0) @interpolate(flat) color: vec4<f32>,
	@location(1) @interpolate(flat) round_corners: vec4<f32>,
	@location(2) coord: vec2<f32>,
	@location(3) @interpolate(flat) extent: vec2<f32>,

	//frag: FragmentInput,
}

/*
struct FragmentInput {
}
*/

alias FragmentInput = VertexOutput;

struct Parameters {
	viewport_inv: vec2<f32>,
}

// var<something, read> mode_fill: bool;
@group(0) @binding(0) var<storage, read> params: Parameters;

@vertex
fn vs_main(input: VertexInput) -> VertexOutput {
	var vertices: array<vec2<f32>, 4> = array(
		vec2(0.0, 0.0),
		vec2(1.0, 0.0),
		vec2(0.0, 1.0),
		vec2(1.0, 1.0),
	);

	let base = vertices[input.index] * input.extent;

	var out: VertexOutput;
	out.position = vec4((base + input.position) * params.viewport_inv * 2.0 - 1.0, 0.0, 1.0);
	out.color = input.color;
	out.round_corners = input.round_corners;
	out.coord = base;
	out.extent = input.extent;

	return out;
}

@fragment
fn fs_fill(input: FragmentInput) -> @location(0) vec4<f32> {
	// Check if the fragment is contained within the area bounded by radius
	// I guess just check every corner

	// Top-left
	{
		let radius = input.round_corners[0];
		let pos = input.coord - vec2(radius, input.extent.y - radius);
		if pos.x < 0.0 && pos.y > 0.0 && pos.x*pos.x + pos.y*pos.y > radius*radius {
			return vec4(0.0);
		}
	}
	// Top-right
	{
		let radius = input.round_corners[1];
		let pos = input.coord - input.extent + vec2(radius);
		if pos.x > 0.0 && pos.y > 0.0 && pos.x*pos.x + pos.y*pos.y > radius*radius {
			return vec4(0.0);
		}
	}
	// Bottom-right
	{
		let radius = input.round_corners[2];
		let pos = input.coord - vec2(input.extent.x - radius, radius);
		if pos.x > 0.0 && pos.y < 0.0 && pos.x*pos.x + pos.y*pos.y > radius*radius {
			return vec4(0.0);
		}
	}
	// Bottom-left
	{
		let radius = input.round_corners[3];
		let pos = input.coord - vec2(radius);
		if pos.x < 0.0 && pos.y < 0.0 && pos.x*pos.x + pos.y*pos.y > radius*radius {
			return vec4(0.0);
		}
	}

	return input.color;
}

@fragment
fn fs_stroke(input: FragmentInput) -> @location(0) vec4<f32> {
	// Check if the fragment is within proximity of the boundary
	// TODO




	return input.color;
}
