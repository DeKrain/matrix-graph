struct VertexOutput {
	@builtin(position) clip: vec4<f32>,
	@location(0) color: vec3<f32>,
}

/*const vertices: array<vec2<f32>, 4> = array(
	vec2<f32>(-0.3, -0.3),
	vec2( 0.3, -0.3),
	vec2( 0.3,  0.3),
	vec2(-0.3,  0.3),
);*/

/*
@vertex
fn vs_main(@builtin(vertex_index) index: u32) -> @builtin(position) vec4<f32> {
	var vertices: array<vec2<f32>, 4> = array(
		vec2( 0.3, -0.3),
		vec2( 0.3,  0.3),
		vec2(-0.3, -0.3),
		vec2(-0.3,  0.3),
	);

	return vec4(vertices[index], 0.0, 1.0);
}

@fragment
fn fs_main(@builtin(position) in: vec4<f32>) -> @location(0) vec4<f32> {
	return vec4(0.3, 0.7, 0.7, 1.0);
}

*/

struct SimpleVertex {
	@location(0) pos: vec3<f32>,
	@location(1) clr: vec3<f32>,
}

@vertex
fn vs_main(vertex: SimpleVertex) -> VertexOutput {
	var out: VertexOutput;
	out.clip = vec4(vertex.pos, 1.0);
	out.color = vertex.clr;
	return out;
}

@fragment
fn fs_main(@location(0) vtx_color: vec3<f32>) -> @location(0) vec4<f32> {
	return vec4(vtx_color, 1.0);
}
