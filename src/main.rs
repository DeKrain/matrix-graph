#![feature(offset_of_nested, float_next_up_down, try_blocks, decl_macro)]

pub(crate) mod future_block;
pub(crate) mod util;
pub(crate) mod display;
pub(crate) mod shader_store;
pub(crate) mod rendering;
pub mod text;
pub mod menu;
pub mod hud;
pub mod level;
pub mod controller;
pub mod physics;

pub(crate) use display::*;

use glyphon::cosmic_text as ct;

use std::{cell::{UnsafeCell, RefCell}, rc::Rc};

use controller::ButtonState;
use winit::{event_loop::{EventLoop, ControlFlow}, window::WindowBuilder, event::{Event, WindowEvent, DeviceEvent}, dpi::PhysicalSize};
use color_eyre::eyre::{self, eyre};

pub(crate) type AnyResult<T> = eyre::Result<T>;
pub(crate) type RcCell<T> = Rc<RefCell<T>>;

use future_block::block_on;
use util::{matrix_to_wgsl, SliceExt};
use controller::{Controller, ButtonMapping};
use rendering::*;
use text::{TextState, TextFrame};
use shader_store::ShaderStore;

pub struct App {
	display: Display,

	shader_store: ShaderStore,

	// pipeline: Option<Box<dyn Pipeline>>,
	background: Box<BackgroundPipeline>,

	text_state: TextState,
	hud_state: Option<Box<hud::UIParts>>,
	debug: DebugDisplay,

	controller: Controller,
	level: Option<Box<level::LevelStage>>,
	menu_stack: Box<menu::OpenMenuStack>,
	pause_menu: Rc<menu::Menu>,
	cached_objects: CachedObjects,

	perf_last_time: Option<std::time::Instant>,
}

pub enum GameMode {
	Playing,
	Exit,
}

pub(crate) struct CachedObjects {
	common_pipeline_data: Rc<CommonPipelineData>,
	part_common: UnsafeCell<Option<Rc<PartPipelineCommon>>>,
	part_placer: UnsafeCell<Option<RcCell<level::PartPlacer>>>,
	part_pipeline: UnsafeCell<Option<Rc<PartPipeline>>>,
	part_wireframe_pipeline: UnsafeCell<Option<Rc<PartWireframePipeline>>>,
	//part_common: Option<Rc<PartPipelineCommon>>,
	//part_placer: Option<RcCell<level::PartPlacer>>,
}

impl CachedObjects {
	pub fn part_placer(&self) -> RcCell<level::PartPlacer> {
		/*unsafe {
			if let Some(part_placer) = (*self.part_placer.get()).clone() {
				part_placer
			} else {
				(*self.part_placer.get()).insert(Rc::new(level::PartPlacer::new())).clone()
			}
		}*/
		unsafe { (*self.part_placer.get()).as_ref().unwrap().clone() }
	}

	pub fn part_common(&self) -> Rc<PartPipelineCommon> {
		/*unsafe {
			if let Some(part_common) = (*self.part_common.get()).clone() {
				part_common
			} else {
				(*self.part_common.get()).insert(Rc::new(PartPipelineCommon::new(app))).clone()
			}
		}*/
		unsafe { (*self.part_common.get()).as_ref().unwrap().clone() }
	}

	pub fn part_pipeline(&self) -> Rc<PartPipeline> {
		unsafe { (*self.part_pipeline.get()).as_ref().unwrap().clone() }
	}

	pub fn part_wireframe_pipeline(&self) -> Rc<PartWireframePipeline> {
		unsafe { (*self.part_wireframe_pipeline.get()).as_ref().unwrap().clone() }
	}

	/*pub fn new(app: &App) -> AnyResult<Self> {
		Ok(CachedObjects {
			part_common: PartPipelineCommon::new(app)?,
			part_placer: level::PartPlacer::new(app)?,
		})
	}*/

	pub const fn new(common_pipeline_data: Rc<CommonPipelineData>) -> Self {
		CachedObjects {
			common_pipeline_data,
			part_common: UnsafeCell::new(None),
			part_placer: UnsafeCell::new(None),
			part_pipeline: UnsafeCell::new(None),
			part_wireframe_pipeline: UnsafeCell::new(None),
		}
	}

	pub fn preload(&self, app: &App) -> AnyResult<()> {
		unsafe {
			self.part_common.get().write(Some(PartPipelineCommon::new(app)?));
			self.part_placer.get().write(Some(level::PartPlacer::new(app)?));
			self.part_pipeline.get().write(Some(PartPipeline::create(&app.display, self.part_common())?));
			self.part_wireframe_pipeline.get().write(Some(PartWireframePipeline::create(&app.display, self.part_common())?));
		}
		Ok(())
	}
}

struct DebugDisplay {
	fps_text: ct::Buffer,
}

impl DebugDisplay {
	fn new(font_system: &mut ct::FontSystem) -> Self {
		let mut fps_text = glyphon::Buffer::new_empty(glyphon::Metrics { font_size: 30.0, line_height: 50.0, });
		fps_text.set_size(font_system, f32::MAX, f32::MAX);
		DebugDisplay {
			fps_text,
		}
	}

	fn display(&mut self, text_state: &mut TextFrame, fps: f64, level: Option<&level::LevelStage>) {
		let text = if let Some(level) = level {
			format!(
"FPS: {fps:.3}
Player pos: {:?}",
				level.player.position,
			)
		} else {
			format!("FPS: {fps:.3}")
		};
		self.fps_text.set_text(text_state.font_system.get_mut(), &text, ct::Attrs::new(), ct::Shaping::Basic);
		text_state.push_area(glyphon::TextArea {
			buffer: &self.fps_text,
			left: 20.0,
			top: 70.0,
			scale: 1.0,
			bounds: glyphon::TextBounds::default(),
			default_color: ct::Color::rgb(0xFF, 0xFF, 0xFF),
		});
	}
}

pub(crate) enum AppEvent {
	SaveLevel,
	LoadLevel,
}

impl App {
	#[inline]
	fn resize(&mut self, new_size: PhysicalSize<u32>) {
		self.display.resize(new_size);
		self.hud_state.as_deref().unwrap().update_viewport(&self.display);
		self.update_view();
	}

	#[inline(always)]
	fn win_id(&self) -> winit::window::WindowId {
		self.display.win.id()
	}

	#[inline(always)]
	pub(crate) unsafe fn display_ref(&self) -> DisplayRef {
		self.display.make_ref()
	}

	#[inline(always)]
	fn cached_objects(&self) -> &CachedObjects {
		//self.cached_objects.as_ref().unwrap()
		//unsafe { self.cached_objects.assume_init_ref() }
		&self.cached_objects
	}

	fn run(mut self: Box<Self>, event_loop: EventLoop<AppEvent>) -> ! {
		self.update_view();
		let mut want_redraw = true;
		let result = event_loop.run(move |event, wt| {
			wt.set_control_flow(ControlFlow::Wait);
			match event {
				Event::WindowEvent {
					window_id,
					ref event,
				} if window_id == self.win_id() => match *event {
					WindowEvent::CloseRequested => {
						wt.exit();
					}
					// Scale factor changes now result in firing `Resized` event afterwards.
					WindowEvent::Resized(new_size) => {
						self.resize(new_size);
					}
					WindowEvent::KeyboardInput { device_id: _, ref event, is_synthetic: _ } => {
						self.kb_input(event);
						if self.controller.button_state().contains(ButtonState::Exit) {
							self.handle_exit();
						}
					}
					WindowEvent::MouseWheel { delta, phase, .. } => {
						let _ = phase;
						#[cfg(any())] {
							let mut stdout = std::io::stdout().lock();
							use std::io::Write;
							let _ = write!(stdout, "\x1b[2K;Mouse wheel: {:?}; phase: {:?}\r", delta, phase);
							let _ = stdout.flush();
						}
						self.mouse_wheel(delta);
					}
					WindowEvent::MouseInput { state, button, .. } => {
						// Propagate directly to level/menu
						use winit::event::ElementState;
						if self.menu_stack.is_open() {
							match state {
								ElementState::Pressed => self.menu_stack.mouse_down(&self.text_state),
								ElementState::Released => self.menu_stack.mouse_up(),
							}
						} else if let Some(level) = self.level.as_deref_mut() {
							level.mouse_input(state, button);
						}
					}
					WindowEvent::CursorMoved { position, .. } if self.menu_stack.is_open() => {
						self.menu_stack.set_cursor_position(position.x as f32, position.y as f32);
					}
					WindowEvent::RedrawRequested => {
						want_redraw = true;
						self.update(wt);
						if let Err(err) = self.render() {
							match err {
								wgpu::SurfaceError::Lost => {
									println!("Swapchain lost; recreating");
									self.display.configure();
								}
								wgpu::SurfaceError::Outdated => {
									println!("Surface config outdated");
								}
								wgpu::SurfaceError::OutOfMemory => panic!("Out of memory!"),
								wgpu::SurfaceError::Timeout => println!("Surface timeout!"),
							}
						}
					}
					_ => {}
				}
				Event::AboutToWait => {
					if want_redraw {
						self.display.win.request_redraw();
						want_redraw = false;
					}
				}
				Event::DeviceEvent { device_id: _, ref event } => match *event {
					DeviceEvent::MouseMotion { delta: (x, y) } => {
						self.controller.handle_mouse_movement(x, y);
					}
					_ => {}
				}
				Event::UserEvent(event) => match event {
					AppEvent::SaveLevel => {
						let Some(ref level) = self.level else { return };
						let file = rfd::FileDialog::new()
							.add_filter("Level file (.json)", &["json"])
							.set_file_name("level.json")
							.save_file();
						if let Some(file) = file {
							let result: AnyResult<()> = try {
								let mut file = std::fs::File::create(file)?;
								level.serialize(level::serial::SerializationOptions {
									writer: &mut file,
									flags: level::serial::SerializationFlags::IncludeCameraRotation,
								})?;
							};

							if let Err(error) = result {
								eprintln!("Warning: serializing level failed: {error}");
							} else {
								println!("Level serialized!");
							}
						}
					}
					AppEvent::LoadLevel => {
						let Some(ref mut level) = self.level else { return };
						let file = rfd::FileDialog::new()
							.add_filter("Level file (.json)", &["json"])
							.pick_file();
						if let Some(file) = file {
							let result: AnyResult<()> = try {
								let mut file = std::fs::File::open(file)?;
								level.deserialize(level::serial::DeserializationOptions {
									reader: &mut file,
								})?;
								self.update_view();
							};

							if let Err(error) = result {
								eprintln!("Warning: deserializing level failed: {error}");
							} else {
								println!("Level deserialized!");
							}
						}
					}
				}
				_ => {}
			}
		});

		use std::process::exit;
		match result {
			Ok(()) => exit(0),
			Err(winit::error::EventLoopError::ExitFailure(status)) => exit(status),
			Err(err) => {
				eprintln!("Failed to run the event loop: {err}");
				exit(1);
			}
		}
	}

	fn kb_input(&mut self, event: &winit::event::KeyEvent) {
		self.controller.handle_keyboard_event(event);
	}

	fn mouse_wheel(&mut self, delta: winit::event::MouseScrollDelta) {
		let (lines_dx, lines_dy) = match delta {
			winit::event::MouseScrollDelta::LineDelta(x, y) => (x as f64, y as f64),
			winit::event::MouseScrollDelta::PixelDelta(pos) => (pos.x / 30.0, pos.y / 30.0),
		};

		self.controller.handle_mouse_scroll(lines_dx, lines_dy)
	}

	fn menu_closed(&mut self) {
		if let Err(err) = self.display.win.set_cursor_grab(winit::window::CursorGrabMode::Locked) {
			eprintln!("Warning: couldn't lock cursor: {err}");
		}
		self.display.win.set_cursor_visible(false);
	}

	fn handle_exit(&mut self) {
		if self.menu_stack.is_open() {
			self.menu_stack.close();
			self.menu_closed();
		} else {
			self.menu_stack.open_root(self.pause_menu.clone(), &self.text_state);
			if let Err(err) = self.display.win.set_cursor_grab(winit::window::CursorGrabMode::None) {
				eprintln!("Warning: couldn't unlock cursor: {err}");
			}
			self.display.win.set_cursor_visible(true);
		}
	}

	fn update(&mut self, wt: &winit::event_loop::EventLoopWindowTarget<AppEvent>) {
		if self.menu_stack.is_open() {
			self.controller.poll();
		} else if let Some(level) = self.level.as_deref_mut() {
			level.update(&mut self.controller, &mut self.text_state);
			self.update_view();
		} else {
			// Poll the state
			self.controller.poll();
		}

		if let Some(state) = self.menu_stack.animate(&self.text_state) {
			use menu::MenuStateChange as S;
			match state {
				S::GameMode(GameMode::Exit) => wt.exit(),
				S::GameMode(GameMode::Playing) => {}
				S::Closed => self.menu_closed(),
			}
		}
	}

	#[inline(always)]
	fn vieport_size(&self) -> (u32, u32) {
		(self.display.config.width, self.display.config.height)
	}

	#[inline]
	fn viewport_size_f(&self) -> (f32, f32) {
		let (w, h) = self.vieport_size();
		(w as f32, h as f32)
	}

	fn update_view(&mut self) {
		use cgmath::{Matrix4, SquareMatrix};

		// Write common shader attributes
		let mut vp_matrix;
		if let Some(level) = self.level.as_deref() {
			let view = level.camera.view_matrix(&level.player);
			let projection = level.camera.projection_matrix(self.viewport_size_f());
			vp_matrix = projection * view;
			matrix_to_wgsl(&mut vp_matrix);
		} else {
			vp_matrix = Matrix4::identity();
		}
		let attrs = CommonShaderData {
			vp_matrix,
		};

		self.display.queue.write_buffer(&self.cached_objects.common_pipeline_data.shared_shader_buffer, 0, attrs.as_bytes());
	}

	fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
		let render_texture = self.display.surface.get_current_texture()?;
		let view = render_texture.texture.create_view(&wgpu::TextureViewDescriptor::default());
		let mut encoder = self.display.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
			label: None,
		});

		let mut level_command = None;
		if let Some(ref mut level) = self.level {
			level_command = level.prepare_buffers(&self.display.device);
		}

		const CLEAR_COLOR: wgpu::Color = wgpu::Color { r: 1.0, g: 1.0, b: 1.0, a: 1.0, };

		let level_render;
		let hud_command;
		{
		let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
			label: None,
			color_attachments: &[Some(wgpu::RenderPassColorAttachment {
				view: &view,
				resolve_target: None,
				ops: wgpu::Operations {
					load: wgpu::LoadOp::Clear(CLEAR_COLOR),
					store: wgpu::StoreOp::Store,
				},
			})],
			depth_stencil_attachment: self.display.depth_stencil_texture
				.as_ref().map(
					|texture| wgpu::RenderPassDepthStencilAttachment {
						view: &texture.view,
						depth_ops: Some(wgpu::Operations {
							load: wgpu::LoadOp::Clear(1.0),
							store: wgpu::StoreOp::Store,
						}),
						stencil_ops: None,
					}
				),
			timestamp_writes: None,
			occlusion_query_set: None,
		});

		self.background.render(&mut render_pass);

		let mut text_frame = self.text_state.new_frame();
		let hud_state = self.hud_state.as_deref_mut().unwrap();
		hud_state.begin_frame(&self.display.device);

		/*if let Some(pipeline) = self.pipeline.as_deref_mut() {
			pipeline.render(&mut render_pass);
		}*/
		if let Some(ref level) = self.level {
			level_render = level.prepare_render();
			level_render.render(unsafe { self.display.make_ref() }, &mut text_frame, hud_state, &mut render_pass);
		}

		if let Some(last_time) = self.perf_last_time.replace(std::time::Instant::now()) {
			let elapsed = last_time.elapsed();

			let fps = 1.0 / elapsed.as_secs_f64();
			self.debug.display(&mut text_frame, fps, self.level.as_deref());
		}

		self.menu_stack.render(unsafe { self.display.make_ref() }, hud_state, &mut text_frame);

		hud_command = hud_state.render(&self.display.device, &mut render_pass);

		if let Err(err) = text_frame.prepare(&self.display) {
			eprintln!("Warning: error text rendering: {err}");
		} else {
			if let Err(err) = self.text_state.render(&mut render_pass) {
				eprintln!("Warning: error rendering text: {err}");
			}
		}
		}

		self.display.queue.submit(level_command.into_iter().chain([hud_command, encoder.finish()]));
		render_texture.present();

		Ok(())
	}
}

#[derive(clap::Parser)]
struct CliArgs {
	/// Enable frame rate test by removing the limit of FPS (using Mailbox strategy)
	#[arg(long)]
	test_framerate: bool,

	#[arg(long, group = "gpu_spec")]
	list_adapters: bool,
	#[arg(long, group = "gpu_spec")]
	gpu_power_preference: Option<CliPowerPreference>,
	#[arg(long, group = "gpu_spec")]
	gpu_adapter_name: Option<String>,
}

#[derive(clap::ValueEnum, Clone, Copy)]
enum CliPowerPreference {
	/// Adapter that uses the least possible power. This is often an integrated GPU.
	LowPower = 1,
	/// Adapter that has the highest performance. This is often a discrete GPU.
	HighPerformance = 2,
}

fn main() -> AnyResult<()> {
	use clap::Parser;

	env_logger::init();
	let _ = color_eyre::install();

	let args = CliArgs::parse();

	let event_loop = winit::event_loop::EventLoopBuilder::with_user_event().build()?;
	let win = WindowBuilder::new()
		.with_title("Matrix Grafx")
		.with_maximized(true)
		.build(&event_loop)?;

	let backends = wgpu::Backends::all();
	let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
		backends,
		flags: wgpu::InstanceFlags::empty(),
		dx12_shader_compiler: wgpu::Dx12Compiler::default(),
		gles_minor_version: wgpu::Gles3MinorVersion::Automatic,
	});
	// TODO: Switch to the safe variant
	let surface = unsafe { instance.create_surface_unsafe(wgpu::SurfaceTargetUnsafe::from_window(&win)?) }?;

	if args.list_adapters {
		println!("Available adapters:");
		for adapter in instance.enumerate_adapters(backends) {
			let info = adapter.get_info();
			let surface_supported = adapter.is_surface_supported(&surface);
			println!("\t{:?} [{:?}]{}", info.name, info.backend, if surface_supported {
				" (surface supported)"
			} else { "" });
		}
		return Ok(());
	}

	enum AdapterSpec {
		Name(String),
		PowerPreference(wgpu::PowerPreference),
	}

	let adapter_spec = if let Some(adapter_name) = args.gpu_adapter_name {
		AdapterSpec::Name(adapter_name)
	} else if let Some(power_preference) = args.gpu_power_preference {
		AdapterSpec::PowerPreference(match power_preference {
			CliPowerPreference::LowPower => wgpu::PowerPreference::LowPower,
			CliPowerPreference::HighPerformance => wgpu::PowerPreference::HighPerformance,
		})
	} else {
		AdapterSpec::PowerPreference(wgpu::PowerPreference::None)
	};

	let adapter = match adapter_spec {
		AdapterSpec::PowerPreference(power_preference) => block_on(instance.request_adapter(
			&wgpu::RequestAdapterOptions {
				power_preference,
				force_fallback_adapter: false,
				compatible_surface: Some(&surface),
			}
		)).ok_or_else(|| eyre!("Expected an adapter to be available"))?,
		AdapterSpec::Name(name) => {
			let mut adapters = instance.enumerate_adapters(backends).into_iter();
			loop {
				match adapters.next() {
					Some(adapter) => {
						if adapter.get_info().name == name {
							break adapter;
						}
					}
					None => return Err(eyre!("Adapter with provided name not found")),
				}
			}
		}
	};

	{
		let info = adapter.get_info();
		println!("Chosen adapter: {}\n\tType: {:?}\n\tDriver: {}\n\tBackend: {:?}", info.name, info.device_type, info.driver, info.backend);
	}

	let (device, queue) = block_on(adapter.request_device(
		&wgpu::DeviceDescriptor {
			label: None,
			required_features: wgpu::Features::empty(),
			required_limits: wgpu::Limits::default(),
		}, None))?;
	device.on_uncaptured_error(Box::new(|error| {
		if let wgpu::Error::Validation { source, description } = error {
			eprintln!("{description}");
			eprintln!("Debug: {source:#?}");
			panic!("Program encountered a validation error");
		}
	}));

	let caps = surface.get_capabilities(&adapter);
	let format = *caps.formats.iter()
		.find(|&format| !format.is_srgb())
		.unwrap_or(&caps.formats[0]);
	println!("Chosen format: {format:?}");
	let size = PhysicalSize::new(800, 800);
	let config = wgpu::SurfaceConfiguration {
		usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
		format,
		width: size.width,
		height: size.height,
		present_mode: if args.test_framerate { wgpu::PresentMode::Mailbox } else { wgpu::PresentMode::Fifo },
		desired_maximum_frame_latency: 1,
		alpha_mode: wgpu::CompositeAlphaMode::Opaque,
		//view_formats: vec![format.remove_srgb_suffix(), format.add_srgb_suffix()],
		view_formats: Vec::new(),
	};

	let depth_stencil_format = rendering::DepthStencilFormat::Depth24Plus;
	let depth_stencil_texture = Some(create_depth_texture(&device, &config, depth_stencil_format));

	let display = Display {
		//event_loop,
		win,
		instance,
		surface,
		adapter,
		device,
		queue,
		config,
		depth_stencil_format,
		depth_stencil_texture,
	};

	let common_pipeline_data = CommonPipelineData::create(&display.device);

	let mut text_state = TextState::new(&display.device, &display.queue, format, display.depth_stencil_state_disabled());

	use std::cell::OnceCell;

	let menu_stack = menu::OpenMenuStack::new(&display.device, &text_state, event_loop.create_proxy());
	let part_placer_handle = Rc::new(OnceCell::new());
	let pause_menu = menu::build_menu![
		"Menu";
		-"Resume";
		#"Options" => {
			"Options";
			-"Back";
		}
		#"Editor" => {
			"Editor menu";
			-"Back";
			@"Grid snap" => {
				use level::PartPlacer;
				struct GridSlider(Rc<OnceCell<RcCell<PartPlacer>>>);
				impl menu::IntControl for GridSlider {
					fn update(&self, value: i32) -> Option<String> {
						let mut placer = (**self.0.get().unwrap()).borrow_mut();
						let value = value as u8;
						placer.snap_level = value;
						Some(PartPlacer::snap_label(value))
					}
					fn get_current(&self) -> (i32, Option<String>) {
						let placer = (**self.0.get().unwrap()).borrow();
						let value = placer.snap_level;
						(value as i32, Some(PartPlacer::snap_label(value)))
					}
				}
				menu::MenuItemKind::IntSlider {
					range: 0..=12,
					increment: std::num::NonZeroU32::MIN,
					target: Box::new(GridSlider(part_placer_handle.clone())),
				}
			};
			!{
				"Save level" => |actions| {
					actions.submit_event(AppEvent::SaveLevel);
				}
				"Load level" => |actions| {
					actions.submit_event(AppEvent::LoadLevel);
				}
			}
		}
		!"Exit" => |actions| {
			actions.change_gamemode(GameMode::Exit);
		}
	];

	if let Err(err) = display.win.set_cursor_grab(winit::window::CursorGrabMode::Locked) {
		eprintln!("Warning: couldn't lock cursor: {err}");
	}
	display.win.set_cursor_visible(false);

	let shader_store = ShaderStore::new();

	let background = BackgroundPipeline::create(&display, &shader_store)?;

	const MOUSE_SENS: f64 = 0.001;
	let mut app = Box::new(App {
		display,
		shader_store,

		// pipeline: None,
		background,

		debug: DebugDisplay::new(text_state.font_system.get_mut()),
		text_state,
		hud_state: None,

		controller: Controller::new(ButtonMapping::default(), (MOUSE_SENS, MOUSE_SENS)),
		level: None,
		menu_stack,
		pause_menu,
		cached_objects: CachedObjects::new(common_pipeline_data),

		perf_last_time: None,
	});

	//app.cached_objects = Some(CachedObjects::new(&app)?);
	app.cached_objects.preload(&app)?;
	app.hud_state = Some(hud::UIParts::new(&app)?);
	//app.pipeline = Some(PartPipeline::create(&app.display, &app.shader_store)?);
	app.level = Some(level::LevelStage::new_example(&app)?);
	part_placer_handle.set(app.cached_objects.part_placer())
		.map_err(drop).expect("Part placer must not have been bound already");

	app.display.configure();
	app.run(event_loop);
}
