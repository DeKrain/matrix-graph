//! Heads-up display - user interface elements rendered seperately from the main game.

use std::cell::UnsafeCell;

use crate::{rendering::{buffers::StreamingBuffer, hud::*}, AnyResult, App};

/// All coordinates use a framebuffer space, where:
/// x: 0.0 - left; width - right
/// y: 0.0 - bottom; height - top
#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct UIRect {
	pub x: f32,
	pub y: f32,
	pub w: f32,
	pub h: f32,
}

impl UIRect {
	#[inline]
	pub fn from_relative(&self, display_dimensions: [u32; 2]) -> Self {
		let [width, height] = display_dimensions;
		let width = width as f32;
		let height = height as f32;
		UIRect {
			x: self.x * width,
			y: self.y * height,
			w: self.w * width,
			h: self.h * height,
		}
	}

	#[inline]
	/// Converts from framebuffer coordinates to coordinates used in the renderer.
	pub fn from_framebuffer_coordinates(&self, height: u32) -> Self {
		UIRect {
			x: self.x,
			y: height as f32 - self.y - self.h,
			w: self.w,
			h: self.h,
		}
	}

	#[inline]
	/// Converts to framebuffer coordinates.
	pub fn to_framebuffer_coordinates(&self, height: u32) -> Self {
		UIRect {
			x: self.x,
			y: height as f32 - self.y + self.h,
			w: self.w,
			h: self.h,
		}
	}

	/// Transforms so that base coordinates are in the display range
	pub fn wrap_around(mut self, display_dimensions: [u32; 2]) -> Self {
		let [width, height] = display_dimensions;
		let width = width as f32;
		let height = height as f32;
		if self.x < 0.0 || self.x >= width {
			self.x = self.x.rem_euclid(width);
		}
		if self.y < 0.0 || self.y >= height {
			self.y = self.y.rem_euclid(height);
		}
		self
	}

	/// Tranforms so that width and height are always non-negative
	pub fn canonalize(mut self) -> Self {
		if self.w < 0.0 {
			self.x += self.w;
			self.w = -self.w;
		}
		if self.h < 0.0 {
			self.y += self.h;
			self.h = -self.h;
		}
		self
	}
}

impl From<UIRect> for glyphon::TextBounds {
	#[inline]
	/// Rect is assumed to be in framebuffer coordinates
	fn from(rect: UIRect) -> Self {
		glyphon::TextBounds {
			left: rect.x as i32,
			top: rect.y as i32,
			right: rect.x as i32 + rect.w as i32,
			bottom: rect.y as i32 + rect.h as i32,
		}
	}
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct UIPoint {
	pub x: f32,
	pub y: f32,
}

impl UIPoint {
	pub const ORIGIN: Self = UIPoint { x: 0.0, y: 0.0 };
}

/// A linear (maybe will be changed) color space color.
#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct UIColor {
	pub r: u8,
	pub g: u8,
	pub b: u8,
	pub a: u8,
}

impl UIColor {
	#[inline]
	pub const fn rgb(r: u8, g: u8, b: u8) -> Self {
		Self { r, g, b, a: 0xFF }
	}

	#[inline]
	pub const fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
		Self { r, g, b, a }
	}
}

/// Values specified for every corner of a rectangle.
/// Usually used for attributes like corner radius.
#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Corners {
	pub top_left: f32,
	pub top_right: f32,
	pub bottom_right: f32,
	pub bottom_left: f32,
}

impl Corners {
	pub const ZERO: Self = Self::repeat(0.0);

	#[inline(always)]
	pub const fn new(
		top_left: f32,
		top_right: f32,
		bottom_right: f32,
		bottom_left: f32,
	) -> Self {
		Self { top_left, top_right, bottom_right, bottom_left }
	}

	#[inline]
	pub const fn repeat(value: f32) -> Self {
		Self { top_left: value, top_right: value, bottom_right: value, bottom_left: value }
	}
}

pub struct UIParts {
	pipeline: UIPartRenderingPipeline,
	display: crate::DisplayRef,
	buffers: UnsafeCell<PartBuffers>,
}

struct PartBuffers {
	//buffer: StreamingBuffer,
	fill_buffer: StreamingBuffer,
	stroke_buffer: StreamingBuffer,
}

pub enum FillMode {
	Fill,
	Stroke,
}

impl UIParts {
	pub fn new(app: &App) -> AnyResult<Box<Self>> {
		let pipeline = UIPartRenderingPipeline::new(&app.display, &app.shader_store)?;

		let fill_buffer = StreamingBuffer::new_empty(&app.display.device, StreamingBuffer::REQUIRED_BUFFER_USAGES.union(wgpu::BufferUsages::VERTEX), None);
		let stroke_buffer = StreamingBuffer::new_empty(&app.display.device, StreamingBuffer::REQUIRED_BUFFER_USAGES.union(wgpu::BufferUsages::VERTEX), None);

		pipeline.update_viewport(&app.display);

		Ok(Box::new(UIParts {
			pipeline,
			display: unsafe { app.display.make_ref() },
			buffers: UnsafeCell::new(PartBuffers {
				fill_buffer,
				stroke_buffer,
			}),
		}))
	}

	#[inline]
	pub(crate) fn update_viewport(&self, display: &crate::display::Display) {
		self.pipeline.update_viewport(display);
	}

	pub fn begin_frame(&mut self, device: &wgpu::Device) {
		self.buffers.get_mut().fill_buffer.begin_transfer(device);
		self.buffers.get_mut().stroke_buffer.begin_transfer(device);
	}

	pub fn draw_rect(&self, rect: UIRect, color: UIColor, round_corners: Corners, fill_mode: FillMode) {
		let buffer = unsafe { match fill_mode {
			FillMode::Fill => &mut (*self.buffers.get()).fill_buffer,
			FillMode::Stroke => &mut (*self.buffers.get()).stroke_buffer,
		} };
		use crate::util::SliceExt;
		buffer.push(self.display.device(), UIPartRepr {
			rect: rect.canonalize(),
			color,
			round_corners,
		}.as_bytes());
	}

	#[inline(always)]
	pub fn fill_rect(&self, rect: UIRect, color: UIColor, round_corners: Corners) {
		self.draw_rect(rect, color, round_corners, FillMode::Fill);
	}

	#[inline(always)]
	pub fn stroke_rect(&self, rect: UIRect, color: UIColor, round_corners: Corners) {
		self.draw_rect(rect, color, round_corners, FillMode::Stroke);
	}

	#[inline]
	pub fn draw_rect_with_outline(
		&self,
		rect: UIRect,
		fill_color: UIColor,
		stroke_color: UIColor,
		round_corners: Corners,
	) {
		self.draw_rect(rect, fill_color, round_corners, FillMode::Fill);
		self.draw_rect(rect, stroke_color, round_corners, FillMode::Stroke);
	}

	#[inline]
	#[must_use]
	pub fn render<'s: 'r, 'r>(&'s mut self, device: &wgpu::Device, render_pass: &mut wgpu::RenderPass<'r>) -> wgpu::CommandBuffer {
		let buffers = self.buffers.get_mut();
		let mut cmd = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
			label: None,
		});
		buffers.fill_buffer.flush(device, &mut cmd);
		buffers.stroke_buffer.flush(device, &mut cmd);
		self.pipeline.render(render_pass, &buffers.fill_buffer, &buffers.stroke_buffer);
		cmd.finish()
	}
}

pub struct TestScene {
	counter: f32,
}

impl TestScene {
	pub const fn new() -> Self {
		Self { counter: 0.0 }
	}

	pub fn render(&mut self, ui: &UIParts) {
		const SCALE: f32 = 0.05 * 0.5;
		ui.fill_rect(UIRect {
			x: 0.3, y: 0.5,
			w: 0.4, h: 0.1,
		}, UIColor::rgba(0x20, 0x20, 0x20, 0xB0),
		Corners {
			top_left: SCALE * (self.counter.cos() + 1.0),
			top_right: SCALE * ((2.0 * self.counter).cos() + 1.0),
			bottom_right: SCALE * ((3.0 * self.counter).cos() + 1.0),
			bottom_left: SCALE * ((4.0 * self.counter).cos() + 1.0),
		});

		self.counter += 0.03;
	}
}
