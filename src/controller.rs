//! Controler for player's input.

use winit::keyboard::KeyCode;

macro_rules! button_mapping {
	([$state_repr:ident] $(
		$name:ident $member:ident ;
	)*) => {
		#[repr(u8)]
		#[derive(Clone, Copy, PartialEq, Eq)]
		pub enum Button {
			$($name,)*
		}

		bitflags::bitflags! {
			#[derive(Clone, Copy, PartialEq, Eq)]
			pub struct ButtonState: $state_repr {
				$(const $name = 1 << Button::$name as u8;)*
			}
		}

		pub struct ButtonMapping {
			$(pub $member: Mapping,)*
		}

		impl ButtonMapping {
			fn iter(&self) -> impl Iterator<Item = (&Mapping, Button)> {
				[
					$((&self.$member, Button::$name),)*
				].into_iter()
			}
		}
	};
}

impl From<Button> for ButtonState {
	#[inline(always)]
	fn from(value: Button) -> Self {
		// We ensure that every ButtonState bit is 1 << Button value
		Self::from_bits_retain(1 << value as u8)
	}
}

impl std::ops::AddAssign<Button> for ButtonState {
	#[inline(always)]
	fn add_assign(&mut self, rhs: Button) {
		*self |= Self::from(rhs)
	}
}

impl std::ops::Add<Button> for ButtonState {
	type Output = Self;
	#[inline(always)]
	fn add(self, rhs: Button) -> Self {
		self | Self::from(rhs)
	}
}

impl std::ops::SubAssign<Button> for ButtonState {
	#[inline(always)]
	fn sub_assign(&mut self, rhs: Button) {
		*self -= Self::from(rhs)
	}
}

impl std::ops::Sub<Button> for ButtonState {
	type Output = Self;
	#[inline(always)]
	fn sub(self, rhs: Button) -> Self {
		self - Self::from(rhs)
	}
}

impl std::ops::BitXorAssign<Button> for ButtonState {
	#[inline(always)]
	fn bitxor_assign(&mut self, rhs: Button) {
		*self ^= Self::from(rhs)
	}
}

impl std::ops::BitXor<Button> for ButtonState {
	type Output = Self;
	#[inline(always)]
	fn bitxor(self, rhs: Button) -> Self {
		self ^ Self::from(rhs)
	}
}

pub struct Mapping {
	pub keycode: KeyCode,
}

impl Mapping {
	#[inline(always)]
	pub const fn new(keycode: KeyCode) -> Self {
		Mapping { keycode }
	}
}

button_mapping! {
	[u16]
	Forward forward ;
	Backward backward ;
	Left left ;
	Right right ;
	Up up ;
	Down down ;

	ToggleFlying toggle_flying ;
	TogglePartPlacer toggle_part_placer ;

	SaveLevel save_level ;

	Select select ;
	Exit exit ;
}

impl ButtonMapping {
	//pub fn get_default_for(layout: winit::event::KeyCode)
}

impl Default for ButtonMapping {
	fn default() -> Self {
		use KeyCode as K;
		ButtonMapping {
			forward: Mapping::new(K::KeyW),
			backward: Mapping::new(K::KeyS),
			left: Mapping::new(K::KeyA),
			right: Mapping::new(K::KeyD),
			up: Mapping::new(K::Space),
			down: Mapping::new(K::ShiftLeft),

			toggle_flying: Mapping::new(K::Enter),
			toggle_part_placer: Mapping::new(K::KeyP),

			save_level: Mapping::new(K::KeyK),

			select: Mapping::new(K::Tab),
			exit: Mapping::new(K::Escape),
		}
	}
}

pub struct Controller {
	button_state: ButtonState,
	previous_state: ButtonState,
	mapping: ButtonMapping,
	mouse_sens: (f64, f64),

	last_mouse: (f64, f64),
	last_scroll: (f64, f64),
}

impl Controller {
	#[inline]
	pub fn new(mapping: ButtonMapping, mouse_sens: (f64, f64)) -> Self {
		Controller {
			button_state: ButtonState::empty(),
			previous_state: ButtonState::empty(),
			mapping,
			mouse_sens,
			last_mouse: (0.0, 0.0),
			last_scroll: (0.0, 0.0),
		}
	}

	#[inline(always)]
	pub fn button_state(&self) -> ButtonState {
		self.button_state
	}

	/// Gets the buttons pressed in this frame and saves the state.
	#[inline]
	pub fn poll_pressed(&mut self) -> ButtonState {
		let pressed = self.button_state - self.previous_state;
		self.previous_state = self.button_state;
		pressed
	}

	pub fn map_key_virt(&self, key: KeyCode) -> ButtonState {
		let mut state = ButtonState::empty();
		for (mapping, button) in self.mapping.iter() {
			if mapping.keycode == key {
				state += button;
			}
		}
		state
	}

	pub fn handle_keyboard_event(&mut self, event: &winit::event::KeyEvent) {
		let winit::keyboard::PhysicalKey::Code(virtual_keycode) = event.physical_key else {
			return;
		};

		let diff = self.map_key_virt(virtual_keycode);
		use winit::event::ElementState;
		match event.state {
			ElementState::Pressed  => self.button_state |= diff,
			ElementState::Released => self.button_state -= diff,
		}
	}

	pub fn handle_mouse_movement(&mut self, raw_dx: f64, raw_dy: f64) {
		let (dx, dy) = (raw_dx * self.mouse_sens.0, raw_dy * self.mouse_sens.1);
		self.last_mouse.0 += dx;
		self.last_mouse.1 += dy;
	}

	pub fn handle_mouse_scroll(&mut self, lines_dx: f64, lines_dy: f64) {
		self.last_scroll.0 += lines_dx;
		self.last_scroll.1 += lines_dy;
	}

	/// Gets cummulated mouse movement and resets the counters
	pub fn poll_mouse_movement(&mut self) -> (f64, f64) {
		std::mem::replace(&mut self.last_mouse, (0.0, 0.0))
	}

	/// Gets cummulated scroll lines and clamps the counters into the 0..1 range
	pub fn poll_mouse_scroll(&mut self) -> (f64, f64) {
		let (dx, dy) = self.last_scroll;
		let lx = dx.trunc();
		let ly = dy.trunc();
		self.last_scroll.0 -= lx;
		self.last_scroll.1 -= ly;
		(lx, ly)
	}

	/// Polls all the properties.
	pub fn poll(&mut self) -> PollInput {
		PollInput {
			buttons_pressed: self.poll_pressed(),
			mouse_movement: self.poll_mouse_movement(),
			mouse_scroll: self.poll_mouse_scroll(),
		}
	}
}

pub struct PollInput {
	pub buttons_pressed: ButtonState,
	pub mouse_movement: (f64, f64),
	pub mouse_scroll: (f64, f64),
}

// External interface
impl Controller {
	pub fn update_camera(&mut self, camera: &mut cgmath::Point3<f32>) -> bool {
		let mut moved = false;
		macro_rules! match_states {
			{
				$($state:ident => [$xdiff:expr, $ydiff:expr, $zdiff:expr],)*
			} => {
				$(
					if self.button_state.intersects(ButtonState::$state) {
						camera.x += $xdiff;
						camera.y += $ydiff;
						camera.z += $zdiff;
						moved = true;
					}
				)*
			};
		}

		const SEN: f32 = 1.0 / 60.0;

		match_states! {
			Forward  => [ 0.0,  0.0, -SEN],
			Backward => [ 0.0,  0.0,  SEN],
			Left     => [-SEN,  0.0,  0.0],
			Right    => [ SEN,  0.0,  0.0],
			Up       => [ 0.0,  SEN,  0.0],
			Down     => [ 0.0, -SEN,  0.0],
		}

		moved
	}
}
