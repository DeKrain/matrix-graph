/*#[derive(Clone, Copy, PartialEq)]
pub struct Position(pub [f32; 3]);*/

use cgmath::{Zero, EuclideanSpace};
use std::{rc::Rc, mem::MaybeUninit};

use glyphon::cosmic_text as ct;

use crate::{
	App,
	AnyResult,
	RcCell,
	CachedObjects,
	rendering::{Pipeline, PartPlacerGhostPipeline},
	display::DisplayRef,
};

pub mod serial;
pub mod part;

use part::PartContainer;

pub type Position = cgmath::Point3<f32>;
pub type Vector = cgmath::Vector3<f32>;
pub type Quaternion = cgmath::Quaternion<f32>;
pub type Matrix4 = cgmath::Matrix4<f32>;

/// Linear color space
#[derive(Clone, Copy, PartialEq)]
pub struct Color(pub [u16; 3]);

pub struct LevelStage {
	pub player: Player,
	pub camera: Camera,
	cached_objects: std::ptr::NonNull<CachedObjects>,
	part_placer: Option<RcCell<PartPlacer>>,
	parts: Vec<Box<Part>>,
	render: LevelRender,
}

enum GroundState {
	FreeFlight,
	Airbone,
	OnGround { part: u32 },
	Jumping,
}

pub struct Player {
	pub position: Position,
	pub velocity: Vector,
	/// How far up should the camera be positioned.
	pub eye_height: f32,

	pub hitbox_half_width: f32,
	/// Must be positive for collision detection to work.
	pub hitbox_height: f32,

	ground_state: GroundState,
	/// Part that can be interacted with.
	interact_part: Option<u32>,
}

pub struct Camera {
	pub yaw: f32,
	/// Must be enclosed in (-τ/4 .. τ/4) for view to work properly.
	pub pitch: f32,
	pub fov: [f32; 2],

	cached_projection: std::cell::UnsafeCell<Option<((f32, f32), Matrix4)>>,
}

impl Player {
	#[inline]
	pub fn camera_position(&self) -> Position {
		let mut cam_pos = self.position;
		cam_pos.y += self.eye_height;
		cam_pos
	}
}

impl Camera {
	#[inline]
	pub fn view_matrix(&self, player: &Player) -> Matrix4 {
		let cam_pos = player.camera_position();
		let cam_dir = self.direction();
		Matrix4::look_to_rh(
			cam_pos,
			cam_dir,
			Vector::unit_y(),
		)
	}

	#[inline]
	fn direction(&self) -> Vector {
		let (yaw_sin, yaw_cos) = self.yaw.sin_cos();
		let (pitch_sin, pitch_cos) = self.pitch.sin_cos();
		Vector {
			x: yaw_sin * pitch_cos,
			y: pitch_sin,
			z: -yaw_cos * pitch_cos,
		}
	}

	#[inline]
	pub fn projection_matrix(&self, viewport: (f32, f32)) -> Matrix4 {
		use cgmath::Rad;

		if let Some((cview, ref cproj)) = unsafe { *self.cached_projection.get() } {
			if cview == viewport {
				return *cproj;
			}
		}
		let (width, height) = viewport;
		let fovy = crate::util::consts::FRAC_TAU_8;
		let proj = cgmath::perspective(
			Rad(fovy),
			width / height,
			0.25, 128.0);
		unsafe { *self.cached_projection.get() = Some((viewport, proj)); }
		proj
	}
}

/// An axis-aligned cuboid, which can be static or programmable
pub struct Part {
	pub(crate) origin: Position,
	pub(crate) extent: Vector,

	color: Color,
	opacity: f32,
	render: PartRender,

	interaction: PartInteraction,
}

pub struct PartRender {
	layer: u32,
	//vertex_offset: u32,
	//index_offset: u32,
	instance_index: u32,
}

pub enum PartInteraction {
	Static,
	Program(Box<PartProgram>),
}

pub struct PartProgram {}

struct LevelRender {
	display: DisplayRef,
	cube_pipeline: Rc<crate::rendering::PartPipeline>,
	wireframe_pipeline: Rc<crate::rendering::PartWireframePipeline>,
	part_buffer: PartContainer,
	highlight_pipeline: crate::rendering::PartHighlightPipeline,
}

impl LevelStage {
	pub fn new(app: &App) -> AnyResult<Box<Self>> {
		use crate::util::consts::FRAC_TAU_8;
		Ok(Box::new(LevelStage {
			player: Player {
				position: Position::origin(),
				velocity: Vector::zero(),
				eye_height: 7.0,

				hitbox_half_width: 3.0,
				hitbox_height: 10.0,

				ground_state: GroundState::Airbone,
				interact_part: None,
			},
			camera: Camera {
				yaw: 0.0,
				pitch: 0.0,
				fov: [FRAC_TAU_8, FRAC_TAU_8],
				cached_projection: std::cell::UnsafeCell::new(None),
			},
			cached_objects: std::ptr::NonNull::from(app.cached_objects()),
			parts: Vec::new(),
			render: LevelRender {
				display: unsafe { app.display_ref() },
				cube_pipeline: app.cached_objects().part_pipeline(),
				wireframe_pipeline: app.cached_objects().part_wireframe_pipeline(),
				part_buffer: PartContainer::new(&app.display.device),
				highlight_pipeline: crate::rendering::PartHighlightPipeline::new(app)?,
			},
			part_placer: Some(app.cached_objects().part_placer()),
		}))
	}

	pub fn new_example(app: &App) -> AnyResult<Box<Self>> {
		let mut stage = Self::new(app)?;
		//stage.add_cube(Position::new(0.3, 0.3, -0.3), Vector::new(0.2, 0.5, 0.3));
		//stage.add_cube(Position::new(2.0, 1.0, -5.0), Vector::new(0.5, 0.5, 0.3));
		//stage.add_cube(Position::new(-1.5, 2.0, -7.0), Vector::new(2.0, 1.5, 4.0));

		stage.add_cube(Position::new(2.5, 2.0, 1.0), Vector::new(5.0, 5.0, 5.0));
		stage.add_cube(Position::new(2.5, 2.0, 6.5), Vector::new(5.0, 5.0, 5.0));

		stage.add_cube(Position::new(15.0, -2.0, 3.0), Vector::new(7.0, 15.0, 20.0));

		// Floor
		stage.add_cube(Position::new(-20.0, -3.0, -20.0), Vector::new(40.0, 1.0, 40.0));
		stage.add_cube(Position::new(25.0, -3.0, -20.0), Vector::new(40.0, 1.0, 40.0));
		Ok(stage)
	}

	pub fn clear_parts(&mut self) {
		self.parts.clear();
		self.render.part_buffer.clear();
	}

	pub fn add_cube(&mut self, origin: Position, extent: Vector) {
		let instance_index = self.parts.len() as u32;
		assert!(instance_index != u32::MAX, "Cannot have that many parts");

		self.render.part_buffer.add_part(
			self.render.display.device(),
			part::PartInstance {
				origin: *origin.as_ref(),
				extent: *extent.as_ref(),
				wireframe_color: [0, 0, 0, 0xFF],
			},
		);
		let part = Box::new(Part {
			origin,
			extent,
			color: Color([u16::MAX; 3]),
			opacity: 1.0,
			render: PartRender {
				layer: 0,
				instance_index,
			},
			interaction: PartInteraction::Static,
		});
		self.parts.push(part);
	}

	pub fn remove_part(&mut self, index: u32) {
		assert!((index as usize) < self.parts.len());
		self.parts.swap_remove(index as usize);
		if let Some(new_part) = self.parts.get(index as usize) {
			self.render.part_buffer.replace_part(index, part::PartInstance {
				origin: *new_part.origin.as_ref(),
				extent: *new_part.extent.as_ref(),
				wireframe_color: [0, 0, 0, 0xFF],
			});
		}
		self.render.part_buffer.truncate(self.parts.len() as u32);
		// Fixup the referenced ground part.
		// If referencing the last part, it has switched place with the removed part.
		if let GroundState::OnGround { part: ref mut ground } = self.player.ground_state {
			if *ground == index {
				self.player.ground_state = GroundState::Airbone;
			} else if *ground == self.parts.len() as u32 {
				*ground = index;
			}
		}
	}

	fn update_input(&mut self, controller: &mut crate::controller::Controller, polled: &crate::controller::PollInput) {
		use crate::util::consts::FRAC_TAU_4;
		// Mouse movement
		let (dyaw, dpitch) = polled.mouse_movement;
		let dpitch = -dpitch;
		/*if dpitch != 0.0 || dyaw != 0.0 {
			println!("Pitch Δ: {dpitch}; Yaw Δ: {dyaw}")
		}*/
		self.camera.yaw += dyaw as f32;
		self.camera.pitch += dpitch as f32;
		if self.camera.pitch.abs() >= FRAC_TAU_4 {
			self.camera.pitch = (FRAC_TAU_4.next_down()).copysign(self.camera.pitch);
		}

		use crate::controller::ButtonState;
		let button_state = controller.button_state();
		const SENS: f32 = 0.02;
		/*
			The coordinate system is right-handed, with ±Y representing height.
			Meaning, for horizontal directions:
			      -Z
			       ^
			       |
			-X <---+---> +X
			       |
			       v
			      +Z
		*/
		let (yaw_sin, yaw_cos) = self.camera.yaw.sin_cos();
		if button_state.contains(ButtonState::Forward) {
			self.player.velocity.x += SENS * yaw_sin;
			self.player.velocity.z -= SENS * yaw_cos;
		}
		if button_state.contains(ButtonState::Backward) {
			self.player.velocity.x -= SENS * yaw_sin;
			self.player.velocity.z += SENS * yaw_cos;
		}
		if button_state.contains(ButtonState::Left) {
			self.player.velocity.x -= SENS * yaw_cos;
			self.player.velocity.z -= SENS * yaw_sin;
		}
		if button_state.contains(ButtonState::Right) {
			self.player.velocity.x += SENS * yaw_cos;
			self.player.velocity.z += SENS * yaw_sin;
		}

		if let GroundState::FreeFlight = self.player.ground_state {
			if button_state.contains(ButtonState::Up) {
				self.player.velocity.y += SENS;
			}
			if button_state.contains(ButtonState::Down) {
				self.player.velocity.y -= SENS;
			}
		} else {
			// Jumping
			if button_state.contains(ButtonState::Up) {
				if let GroundState::OnGround{..} = self.player.ground_state {
					self.player.ground_state = GroundState::Jumping;
				}
			}
		}

		let pressed = polled.buttons_pressed;
		if pressed.contains(ButtonState::ToggleFlying) {
			match self.player.ground_state {
				GroundState::FreeFlight => self.player.ground_state = GroundState::Airbone,
				_ => self.player.ground_state = GroundState::FreeFlight,
			}
		}

		if polled.buttons_pressed.contains(ButtonState::TogglePartPlacer) {
			match self.part_placer {
				Some(_) => self.part_placer = None,
				None => self.part_placer = unsafe { Some(self.cached_objects.as_ref().part_placer()) },
			}

			self.player.interact_part = None;
		}

		if pressed.contains(ButtonState::SaveLevel) {
			let result: AnyResult<()> = try {
				let mut file = std::fs::File::create("level.json")?;
				self.serialize(serial::SerializationOptions {
					writer: &mut file,
					flags: serial::SerializationFlags::IncludeCameraRotation,
				})?;
			};

			if let Err(error) = result {
				eprintln!("Warning: serializing level failed: {error}");
			} else {
				println!("Level serialized!");
			}
		}
	}

	pub fn update(&mut self, controller: &mut crate::controller::Controller, text_state: &mut crate::text::TextState) {
		let polled = controller.poll();
		self.update_input(controller, &polled);

		// Physics stuff

		// Deceleration
		self.player.velocity.x *= 0.95;
		self.player.velocity.z *= 0.95;

		const GRAVITY_ACCELERATION: f32 = 0.01;
		const JUMP_VELOCITY: f32 = 0.2;

		match self.player.ground_state {
			GroundState::FreeFlight => self.player.velocity.y *= 0.95,
			GroundState::Airbone => {
				// Apply gravity
				self.player.velocity.y -= GRAVITY_ACCELERATION;
			}
			GroundState::OnGround{..} => {}
			GroundState::Jumping => {
				// Start jumping
				self.player.velocity.y = JUMP_VELOCITY;
				self.player.ground_state = GroundState::Airbone;
			}
		}

		const LIMIT: f32 = 0.5;

		// Velocity limit
		if self.player.velocity.x.abs() > LIMIT {
			self.player.velocity.x = LIMIT.copysign(self.player.velocity.x);
		}
		if self.player.velocity.z.abs() > LIMIT {
			self.player.velocity.z = LIMIT.copysign(self.player.velocity.z);
		}
		match self.player.ground_state {
			GroundState::FreeFlight => {
				if self.player.velocity.y.abs() > LIMIT {
					self.player.velocity.y = LIMIT.copysign(self.player.velocity.y);
				}
			}
			GroundState::Airbone => {
				// Ensure terminal falling speed
				if self.player.velocity.y < -LIMIT {
					self.player.velocity.y = -LIMIT;
				}
			}
			_ => {}
		}

		// Check collisions
		match self.player.ground_state {
			GroundState::Airbone if self.player.velocity.y <= 0.0 => {
				for (id, part) in self.parts.iter().enumerate() {
					//if let Some(collision) = self.player.collides_part(part) {
					if part.contains_point(self.player.position) {
						self.player.position.y = part.origin.y.max(part.origin.y + part.extent.y);
						self.player.ground_state = GroundState::OnGround { part: id as u32 };
						self.player.velocity.y = 0.0;
					}
				}
			}
			GroundState::OnGround { part: ref mut ground } => {
				'check: {
					let mut collide_pos = self.player.position;
					collide_pos.y -= 0.1;

					if self.parts[*ground as usize].contains_point(collide_pos) {
						// Still on ground
						break 'check;
					}

					for (part_id, part) in self.parts.iter().enumerate() {
						if part.contains_point(collide_pos) {
							// Found new ground
							self.player.position.y = part.origin.y.max(part.origin.y + part.extent.y);
							*ground = part_id as u32;
							break 'check;
						}
					}

					// No floor found. Start falling.
					self.player.ground_state = GroundState::Airbone;
				}
			}
			_ => {}
		}

		// Apply velocity
		self.player.position += self.player.velocity;

		const PLAYER_INTERACT_RADIUS: f32 = 20.0;

		// Update part placer
		if let Some(part_placer) = self.part_placer.as_deref() {
			part_placer.borrow_mut().update(self, &polled, text_state);
		} else {
			// Find a selected part
			self.player.interact_part = self.raycast_part(self.player.camera_position(), self.camera.direction(), PLAYER_INTERACT_RADIUS);
		}
	}

	pub fn mouse_input(&mut self, state: winit::event::ElementState, button: winit::event::MouseButton) {
		use winit::event::{ElementState, MouseButton};
		match state {
			ElementState::Pressed => match button {
				MouseButton::Left => if let Some(part_placer) = self.part_placer.as_deref() {
					let PartPlacer {
						cube_origin,
						cube_extent,
						..
					} = *part_placer.borrow();
					self.add_cube(cube_origin, cube_extent);
				}
				MouseButton::Right => if let Some(part_placer) = self.part_placer.as_deref() {
					part_placer.borrow_mut().snap_param();
				} else if let Some(interact_part) = self.player.interact_part {
					self.player.interact_part = None;
					self.remove_part(interact_part);
				}
				_ => {}
			}
			ElementState::Released => {}
		}
	}

	/// Tries to Find a part first intersected by a ray of at most specified length.
	fn raycast_part(&self, origin: Position, direction: Vector, max_radius: f32) -> Option<u32> {
		// A simple approach is to iterate over all parts and track the closest one found.
		let (mut part_idx, mut intersect_radius) = (None, max_radius);
		for (part, current_idx) in self.parts.iter().zip(0..) {
			if let Some(ray) = part.intersects_ray(origin, direction) {
				if ray.radius <= intersect_radius {
					part_idx = Some(current_idx);
					intersect_radius = ray.radius;
				}
			}
		}

		part_idx
	}

	#[must_use]
	pub fn prepare_buffers(&mut self, device: &wgpu::Device) -> Option<wgpu::CommandBuffer> {
		self.render.part_buffer.flush_updates(device)
	}

	pub fn prepare_render<'s: 'r, 'r>(&'s self) -> LevelRenderReady<'s, 'r> {
		LevelRenderReady {
			level: &self.render,
			part_placer: self.part_placer.as_deref().map(|p| p.borrow()),
			highlight_part: if let Some(idx) = self.player.interact_part {
				let part = &self.parts[idx as usize];
				Some((part.origin, part.extent))
			} else { None },
		}
	}
}

pub struct LevelRenderReady<'s, 'r> {
	level: &'s LevelRender,
	part_placer: Option<std::cell::Ref<'r, PartPlacer>>,
	highlight_part: Option<(Position, Vector)>,
}

impl<'s: 'r, 'r: 'r2, 'r2> LevelRenderReady<'s, 'r> {
	pub fn render(&'s self, display: crate::DisplayRef, text_state: &mut crate::TextFrame, hud_state: &crate::hud::UIParts, render_pass: &mut wgpu::RenderPass<'r2>) {
		let render = self.level;
		render.cube_pipeline.render(render_pass, render.part_buffer.full_slice(), render.part_buffer.part_count());
		render.wireframe_pipeline.render(render_pass, render.part_buffer.full_slice(), render.part_buffer.part_count());

		if let Some(part_placer) = self.part_placer.as_ref() {
			part_placer.render(display, text_state, hud_state, render_pass);
		} else if let Some(highlight) = self.highlight_part {
			let queue = display.queue();

			// Upload buffer
			use crate::util::SliceExt;
			let data = crate::rendering::PartHighlightAttributes {
				origin: highlight.0.into(),
				extent: highlight.1.into(),
				color: 0xFFA0FF00,
				_pad: MaybeUninit::uninit(),
			};
			queue.write_buffer(&render.highlight_pipeline.buffer, 0, data.as_bytes());

			render.highlight_pipeline.render(render_pass);
		}
	}
}

pub struct PartPlacer {
	ghost_pipeline: PartPlacerGhostPipeline,

	cube_origin: Position,
	cube_extent: Vector,
	camera_distance: f32,
	ghost_opacity: f32,

	selected_menu_item: u32,
	menu_buffer: ct::Buffer,

	pub(crate) snap_level: u8,
}

impl PartPlacer {
	pub(crate) fn new(app: &App) -> AnyResult<RcCell<Self>> {
		let ghost_pipeline = PartPlacerGhostPipeline::new(app)?;

		let mut menu_buffer = ct::Buffer::new_empty(ct::Metrics { font_size: 25.0, line_height: 20.0 });
		menu_buffer.set_size(&mut app.text_state.font_system.borrow_mut(), f32::MAX, f32::MAX);
		let attrs = ct::Attrs::new();
		menu_buffer.lines.extend([
			ct::BufferLine::new(Self::MENU_DISTANCE, ct::AttrsList::new(attrs), ct::Shaping::Basic),
			ct::BufferLine::new(Self::MENU_EXTENT_X, ct::AttrsList::new(attrs), ct::Shaping::Basic),
			ct::BufferLine::new(Self::MENU_EXTENT_Y, ct::AttrsList::new(attrs), ct::Shaping::Basic),
			ct::BufferLine::new(Self::MENU_EXTENT_Z, ct::AttrsList::new(attrs), ct::Shaping::Basic),
		]);

		Ok(Rc::new(std::cell::RefCell::new(PartPlacer {
			ghost_pipeline,
			cube_origin: Position { x: 0.0, y: 0.0, z: 0.0 },
			cube_extent: Vector { x: 3.0, y: 3.0, z: 3.0 },
			camera_distance: 7.0,
			ghost_opacity: 0.6,
			selected_menu_item: 0,
			menu_buffer,
			snap_level: 0,
		})))
	}

	const MENU_DISTANCE: &'static str = "Distance from camera: ";
	const MENU_EXTENT_X: &'static str = "Extent X: ";
	const MENU_EXTENT_Y: &'static str = "Extent Y: ";
	const MENU_EXTENT_Z: &'static str = "Extent Z: ";
	const MENU_ITEM_COUNT: u32 = 4;

	fn update(&mut self, level: &LevelStage, polled: &crate::controller::PollInput, text_state: &mut crate::text::TextState) {
		let snap_value = (self.snap_level != 0).then(|| Self::get_snap_value(self.snap_level));

		// Change value before selection
		let value_dt = polled.mouse_scroll.1;
		if value_dt != 0.0 {
			const DEFAULT_SCALE: f32 = 0.25;
			let scaled = value_dt as f32 * snap_value.unwrap_or(DEFAULT_SCALE);
			match self.selected_menu_item {
				0 => self.camera_distance += scaled,
				1 => self.cube_extent.x += scaled,
				2 => self.cube_extent.y += scaled,
				3 => self.cube_extent.z += scaled,
				_ => {}
			}
		}

		// Calculate the ghost's cube origin
		let mut origin = level.player.camera_position() + self.camera_distance * level.camera.direction();
		// Snap
		if let Some(snap) = snap_value {
			origin = origin.map(|coord| (coord / snap).round() * snap);
		}
		self.cube_origin = origin;

		if polled.buttons_pressed.contains(crate::controller::ButtonState::Select) {
			self.selected_menu_item += 1;
			if self.selected_menu_item >= Self::MENU_ITEM_COUNT {
				self.selected_menu_item = 0;
			}
		}

		// Must use constructor syntax because the functions aren't const
		static MENU_FIELD_ATTR: ct::Attrs<'static> = ct::Attrs {
			color_opt: Some(ct::Color::rgb(0xA0, 0xA0, 0x30)),
			family: ct::Family::Monospace,
			stretch: ct::Stretch::Normal,
			style: ct::Style::Normal,
			weight: ct::Weight::NORMAL,
			metadata: 0,
		};

		static MENU_DEFAULT_ATTR: ct::Attrs<'static> = ct::Attrs {
			color_opt: None,
			family: ct::Family::Monospace,
			stretch: ct::Stretch::Normal,
			style: ct::Style::Normal,
			weight: ct::Weight::NORMAL,
			metadata: 0,
		};
		static MENU_SELECTED_ATTR: ct::Attrs<'static> = ct::Attrs {
			color_opt: Some(ct::Color::rgb(0x20, 0x10, 0xC0)),
			family: ct::Family::Monospace,
			stretch: ct::Stretch::Normal,
			style: ct::Style::Normal,
			weight: ct::Weight::NORMAL,
			metadata: 0,
		};

		// Update menu text
		for (idx, line) in self.menu_buffer.lines.iter_mut().enumerate() {
			macro_rules! format_and_set_suffix {
				($prefix:path, $format:literal $(,$args:expr)* $(,)?) => {
					{
						let mut suffix = line.split_off($prefix.len());
						if idx == self.selected_menu_item as usize {
							line.set_attrs_list(ct::AttrsList::new(MENU_SELECTED_ATTR));
						} else {
							line.set_attrs_list(ct::AttrsList::new(MENU_DEFAULT_ATTR));
						}
						suffix.set_text(format!($format $(,$args)*), ct::AttrsList::new(MENU_FIELD_ATTR));
						line.append(suffix);
					}
				};
			}
			match idx {
				0 => format_and_set_suffix!(Self::MENU_DISTANCE, "{}", self.camera_distance),
				1 => format_and_set_suffix!(Self::MENU_EXTENT_X, "{}", self.cube_extent.x),
				2 => format_and_set_suffix!(Self::MENU_EXTENT_Y, "{}", self.cube_extent.y),
				3 => format_and_set_suffix!(Self::MENU_EXTENT_Z, "{}", self.cube_extent.z),
				_ => {}
			}
		}
		self.menu_buffer.shape_until_scroll(&mut text_state.font_system.get_mut());
	}

	fn snap_param(&mut self) {
		if self.snap_level == 0 { return }
		let snap = Self::get_snap_value(self.snap_level);

		let control = match self.selected_menu_item {
			0 => &mut self.camera_distance,
			1 => &mut self.cube_extent.x,
			2 => &mut self.cube_extent.y,
			3 => &mut self.cube_extent.z,
			_ => return,
		};

		*control = (*control / snap).round() * snap;
	}

	fn render<'s: 'r, 'r>(&'s self, display: crate::DisplayRef, text_state: &mut crate::TextFrame, hud_state: &crate::hud::UIParts, render_pass: &mut wgpu::RenderPass<'r>) {
		let queue = display.queue();

		// Upload buffer
		use crate::util::SliceExt;
		let data = crate::rendering::PartGhostAttributes {
			origin: self.cube_origin.into(),
			extent: self.cube_extent.into(),
			opacity: self.ghost_opacity,
			_pad: MaybeUninit::uninit(),
		};
		queue.write_buffer(&self.ghost_pipeline.buffer, 0, data.as_bytes());

		self.ghost_pipeline.render(render_pass);

		use crate::hud::{UIRect, UIColor, Corners};
		let bounds = UIRect {
			x: 10.0,
			y: 240.0,
			w: 430.0,
			h: 200.0,
		};
		hud_state.fill_rect(bounds.from_framebuffer_coordinates(display.dimensions()[1]), UIColor::rgba(0x20, 0x20, 0x20, 0xB0), Corners::repeat(12.0));
		text_state.push_area(glyphon::TextArea {
			buffer: &self.menu_buffer,
			left: 20.0,
			top: 250.0,
			scale: 1.0,
			bounds: bounds.into(),
			//bounds: glyphon::TextBounds::default(),
			default_color: ct::Color::rgb(0xFF, 0xFF, 0xFF),
		});
	}

	#[inline]
	pub fn get_snap_value(level: u8) -> f32 {
		(1 << level) as f32 / 512.0 //4096.0
	}

	pub fn snap_label(level: u8) -> String {
		if level == 0 {
			return "Disabled".to_owned();
		}
		let vf = Self::get_snap_value(level);
		format!("snap {vf} (level {level})")
	}
}
