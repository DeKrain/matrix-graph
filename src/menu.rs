use std::ops::RangeInclusive;
use std::rc::Rc;

use crate::{
	controller as cont,
	text::{TextState, TextLine, self},
	hud::{self, UIRect, UIPoint},
};

use glyphon::cosmic_text as ct;
use glyphon as gp;
use ct::{Buffer, BufferLine, AttrsList, Attrs};
use winit::event_loop::EventLoopProxy;


pub struct Menu {
	pub title: TextLine,
	// Will become Layout in the future
	pub items: Vec<MenuItem>,
}

pub struct MenuItem {
	pub title: Option<TextLine>,
	pub kind: MenuItemKind,
}

pub trait IntControl {
	/// Display can be customized by returning [`Some`] string.
	fn update(&self, value: i32) -> Option<String>;

	fn get_current(&self) -> (i32, Option<String>);
}

pub enum MenuItemKind {
	Label,
	FloatSlider {
		range: RangeInclusive<f32>,
		update: Box<dyn Fn(f32)>,
	},
	IntSlider {
		range: RangeInclusive<i32>,
		increment: std::num::NonZeroU32,
		target: Box<dyn IntControl>,
	},
	ButtonRow {
		buttons: Vec<ButtonItem>,
	},
	KeyBinding {
		target: cont::Button,
		//mapping: cont::Mapping,
	},
}

pub type ButtonAction = dyn Fn(MenuActions);

pub struct ButtonItem {
	pub title: TextLine,
	pub action: Box<ButtonAction>,
}

impl ButtonItem {
	/// An action that winds the menu back
	pub fn action_back(menu: MenuActions) {
		menu.pop_entry();
	}
}


pub struct OpenMenuStack {
	// text_state: *mut TextState,
	root: Option<Rc<Menu>>,
	animation_state: OpeningMenuState,
	stack: Vec<OpenMenu>,
	event_proxy: EventLoopProxy<crate::AppEvent>,
	title_bar: Buffer,
	empty_key_bind_buffer: Buffer,
	commited_action: Option<CommitedAction>,

	last_width: f32,
	last_height: f32,
	cursor_position: UIPoint,
	need_research: bool,
	item_mouse_held: bool,
}

enum OpeningMenuState {
	Closed,
	Opening { progress: f32 },
	Open,
	Closing { progress: f32 },
}

enum CommitedAction {
	Navigation,
	GameMode(crate::GameMode),
}

pub enum MenuStateChange {
	Closed,
	GameMode(crate::GameMode),
}

struct OpenMenu {
	/// Must be bound to owned menu
	menu: &'static Menu,
	/// Indexes the layout item currently selected.
	/// Value of [`u32::MAX`] aka `!0` signifies that nothing is selected;
	/// it is ensured that this value is never a valid index.
	item_selected: u32,
	/// For button row navigation, ignored otherwise
	button_selected: u32,
	layout: Box<[MenuLayoutItem]>,
}

enum MenuLayoutItem {
	Label {
		position: UIPoint,
		buffer: Buffer,
	},
	Button {
		bb: UIRect,
		addr: u32,
		kind: LayoutButtonKind,
	},
	IntSlider {
		bb: UIRect,
		addr: u32,
		value: i32,
		//display: String,
		label_buffer: Buffer,
		notch_spacing: f32,
		notches: std::num::NonZeroU32,
	},
}

enum LayoutButtonKind {
	Regular {
		row_index: u32,
		label: Buffer,
	},
	KeyBind {
		label: Option<Buffer>,
	},
}

impl OpenMenuStack {
	pub fn new(device: &wgpu::Device, text_state: &TextState, event_proxy: EventLoopProxy<crate::AppEvent>) -> Box<Self> {
		let title_bar = text_state.create_buffer_width(40.0, 30.0, 600.0);
		let mut empty_key_bind_buffer = text_state.create_buffer_width(20.0, 20.0, 200.0);
		empty_key_bind_buffer.lines.push(TextLine::new("-= Set binding =-", AttrsPreset::Button)
			.build_buffer_line()
			.align(ct::Align::Center).0);
		empty_key_bind_buffer.shape_until_scroll(&mut *text_state.font_system.borrow_mut());
		#[cfg(any())]
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label: Some("Menu buffer"),
			size: 0,
			usage: wgpu::BufferUsages::STORAGE,
			mapped_at_creation: false,
		});
		Box::new(OpenMenuStack {
			// text_state,
			root: None,
			animation_state: OpeningMenuState::Closed,
			stack: Vec::new(),
			event_proxy,
			title_bar,
			empty_key_bind_buffer,
			commited_action: None,
			last_width: 0.0,
			last_height: 0.0,
			cursor_position: UIPoint::ORIGIN,
			need_research: false,
			item_mouse_held: false,
		})
	}

	#[inline]
	pub fn is_open(&self) -> bool {
		match self.animation_state {
			OpeningMenuState::Open | OpeningMenuState::Opening {..} => true,
			OpeningMenuState::Closed | OpeningMenuState::Closing {..} => false,
		}
	}

	pub fn open_root(&mut self, root: Rc<Menu>, text_state: &TextState) {
		self.item_mouse_held = false;
		let root = self.root.insert(root);
		match self.animation_state {
			OpeningMenuState::Closed => self.animation_state = OpeningMenuState::Opening { progress: 0.0 },
			OpeningMenuState::Closing { progress } => self.animation_state = OpeningMenuState::Opening { progress: 1.0 - progress },
			_ => {}
		}
		self.stack.clear();
		Self::push_entry(&mut self.stack, root, &mut self.title_bar, text_state);
	}

	pub fn close(&mut self) {
		match self.animation_state {
			OpeningMenuState::Open => self.animation_state = OpeningMenuState::Closing { progress: 0.0 },
			OpeningMenuState::Opening { progress } => self.animation_state = OpeningMenuState::Closing { progress: 1.0 - progress },
			_ => {}
		}
		// self.root = None;
		// self.stack.clear();
	}

	pub fn animate(&mut self, text_state: &TextState) -> Option<MenuStateChange> {
		const SPEED: f32 = 16.0/256.0;
		match self.animation_state {
			OpeningMenuState::Opening { ref mut progress } => {
				*progress += SPEED;
				if *progress >= 1.0 {
					self.animation_state = OpeningMenuState::Open;
				}
				self.need_research = true;
			}
			OpeningMenuState::Closing { ref mut progress } => {
				*progress += SPEED;
				if *progress >= 1.0 {
					self.animation_state = OpeningMenuState::Closed;
					self.root = None;
					self.stack.clear();
				}
			}
			_ => {}
		}

		if self.need_research {
			self.need_research = false;
			self.update_cursor_action(text_state);
		}

		self.commited_action.take().and_then(|a| match a {
			CommitedAction::GameMode(mode) => Some(MenuStateChange::GameMode(mode)),
			CommitedAction::Navigation if !self.is_open() => Some(MenuStateChange::Closed),
			_ => None,
		})
	}

	pub fn set_cursor_position(&mut self, buf_x: f32, buf_y: f32) {
		self.cursor_position = UIPoint { x: buf_x, y: buf_y };
		self.need_research = true;
	}

	fn layout_cursor(&self) -> Option<UIPoint> {
		let offset = match self.animation_state {
			OpeningMenuState::Open => 0.0,
			OpeningMenuState::Opening { progress } => progress - 1.0,
			OpeningMenuState::Closing { progress } => -progress,
			OpeningMenuState::Closed => return None,
		};
		let begin_x = 0.5 * self.last_width - 300.0;
		// let ui_top_y = self.last_height * 0.9 + offset;
		Some(UIPoint {
			x: self.cursor_position.x - begin_x,
			y: self.cursor_position.y + (-0.1 + 0.3 * offset) * self.last_height,
		})
	}

	fn update_cursor_action(&mut self, text_state: &TextState) {
		let Some(cursor) = self.layout_cursor()
			else { return };
		let Some(menu) = self.stack.last_mut()
			else { return };

		if self.item_mouse_held {
			menu.update_held_item(cursor, text_state);
		} else {
			menu.find_item_under_cursor(cursor);
		}
	}
}

impl OpenMenu {
	fn find_item_under_cursor(&mut self, cursor: UIPoint) {
		self.item_selected = !0;
		for (item, index) in self.layout[..].iter().zip(0u32..) {
			match item {
				MenuLayoutItem::Label {..} => {}
				| MenuLayoutItem::Button { bb, .. }
				| MenuLayoutItem::IntSlider { bb, .. } => {
					if (bb.x ..= bb.x + bb.w).contains(&cursor.x) && (bb.y ..= bb.y + bb.h).contains(&cursor.y) {
						self.item_selected = index;
						break;
					}
				}
			}
		}
	}

	fn update_held_item(&mut self, cursor: UIPoint, text_state: &TextState) {
		match self.layout[self.item_selected as usize] {
			MenuLayoutItem::IntSlider {
				ref bb,
				addr,
				ref mut value,
				ref mut label_buffer,
				notch_spacing,
				notches, ..
			} => {
				// Find the closest step value to cursor
				let pos = cursor.x - bb.x;
				let max_step = (notches.get() - 1) as f32;

				let mut step_pos = if notches.get() == 1 { 0.0 } else { pos / notch_spacing };
				step_pos = step_pos.clamp(0.0, max_step).round();
				let step = step_pos as u32;

				let MenuItemKind::IntSlider {
					ref range,
					increment,
					ref target, ..
				} = self.menu.items[addr as usize].kind
					else { unreachable!() };

				let new_val = *range.start() + i32::try_from(increment.get() * step).unwrap();
				if new_val != *value {
					*value = new_val;
					let display = target.update(new_val)
						.unwrap_or_else(|| new_val.to_string());

					label_buffer.lines.clear();
					label_buffer.lines.push(TextLine::new(display, AttrsPreset::Item).buffer_line());
					label_buffer.shape_until_scroll(&mut text_state.font_system.borrow_mut());
				}
			}
			_ => {}
		}
	}
}

impl OpenMenuStack {
	pub fn render(&mut self, display: crate::display::DisplayRef, hud_state: &hud::UIParts, text_state: &mut text::TextFrame) {
		let progress = match self.animation_state {
			OpeningMenuState::Open => 0.0,
			OpeningMenuState::Opening { progress } => progress - 1.0,
			OpeningMenuState::Closing { progress } => -progress,
			OpeningMenuState::Closed => return,
		};

		let [width, height] = display.dimensions();
		let width = width as f32;
		let height = height as f32;
		self.last_width = width;
		self.last_height = height;
		let offset = progress * height * 0.3;
		let top_y = height * 0.1 - offset;
		let ui_top_y = height * 0.9 + offset;
		let bottom_y = height * 0.3 + offset;
		let begin_x = 0.5 * width - 300.0;

		use hud::{UIColor, Corners};
		const BACKGROUND_COLOR: UIColor = UIColor::rgba(0x40, 0x20, 0x18, 0xD0);
		const BUTTON_COLOR: UIColor = UIColor::rgb(0x20, 0x70, 0);
		const BUTTON_HIGHLIGHTED_COLOR: UIColor = UIColor::rgb(0x30, 0xD0, 0x50);
		const BUTTON_STROKE_COLOR: UIColor = UIColor::rgb(0, 0, 0);
		const BUTTON_CORNERS: Corners = Corners::repeat(5.0);
		const TITLE_COLOR: ct::Color = ct::Color::rgb(0x50, 0xC0, 0x90);
		const BUTTON_TEXT_COLOR: ct::Color = ct::Color::rgb(0xFF, 0xFF, 0xFF);
		const LABEL_COLOR: ct::Color = ct::Color::rgb(0x20, 0xA0, 0xC0);
		const SLIDER_BG_COLOR: UIColor = UIColor::rgba(0xA0, 0x70, 0x20, 0xD0);
		const SLIDER_BAR_COLOR: UIColor = UIColor::rgb(0xA0, 0xA0, 0xA0);
		const SLIDER_HANDLE_COLOR: UIColor = UIColor::rgba(0x20, 0x80, 0xC0, 0xC0);
		const SLIDER_HIGHLIGHTED_COLOR: UIColor = UIColor::rgba(0x40, 0xC0, 0xD0, 0xFF);
		const SLIDER_LABEL_COLOR: ct::Color = ct::Color::rgb(0x40, 0xFF, 0xD8);

		let mut bg_color = BACKGROUND_COLOR;
		bg_color.a = ((1.0 + progress * 0.7) * bg_color.a as f32) as u8;
		hud_state.fill_rect(UIRect {
			x: begin_x,
			y: bottom_y,
			w: 600.0,
			h: 0.6 * height,
		}, bg_color, Corners::default());
		text_state.push_area(gp::TextArea {
			buffer: &self.title_bar,
			left: begin_x,
			top: top_y + 10.0,
			scale: 1.0,
			bounds: gp::TextBounds::default(),
			default_color: TITLE_COLOR,
		});

		let transform_rect = { #[inline] |mut rect: UIRect| {
			rect.x += begin_x;
			rect.y = ui_top_y - rect.y - rect.h;
			rect
		} };

		let transform_pos = { #[inline] |mut pos: UIPoint| {
			pos.x += begin_x;
			pos.y += top_y;
			pos
		} };

		let menu = self.stack.last().unwrap();
		for (item, index) in menu.layout[..].iter().zip(0u32..) {
			match item {
				MenuLayoutItem::Label { position, buffer } => {
					let position = transform_pos(*position);
					text_state.push_area(gp::TextArea {
						buffer,
						left: position.x,
						top: position.y,
						scale: 1.0,
						bounds: gp::TextBounds::default(),
						default_color: LABEL_COLOR,
					});
				}
				MenuLayoutItem::Button { bb, kind, .. } => {
					let ty = bb.y + top_y;
					let bb = transform_rect(*bb);
					hud_state.draw_rect_with_outline(bb,
						if menu.item_selected != index { BUTTON_COLOR } else { BUTTON_HIGHLIGHTED_COLOR },
						BUTTON_STROKE_COLOR, BUTTON_CORNERS);

					let label = match kind {
						LayoutButtonKind::Regular { label, .. } => label,
						LayoutButtonKind::KeyBind { label, .. } => if let Some(ref label) = label {
							// TODO: Check if the button is bound
							if true {
								label
							} else {
								&self.empty_key_bind_buffer
							}
						} else {
							&self.empty_key_bind_buffer
						}
					};

					text_state.push_area(gp::TextArea {
						buffer: label,
						left: bb.x + 5.0,
						top: ty,
						scale: 1.0,
						bounds: gp::TextBounds::default(),
						default_color: BUTTON_TEXT_COLOR,
					});
				}
				MenuLayoutItem::IntSlider { bb, addr, value, notch_spacing, notches, label_buffer } => {
					use crate::util::Delerp;

					let ty = bb.y + top_y;
					let bb = transform_rect(*bb);

					// Bounding area
					hud_state.draw_rect(bb, SLIDER_BG_COLOR, Corners::ZERO, hud::FillMode::Fill);

					let range = match self.stack.last().unwrap().menu.items[*addr as usize].kind {
						MenuItemKind::IntSlider { ref range, .. } => range,
						_ => unreachable!(),
					};

					const BAR_WIDTH: f32 = 3.0;
					const HANDLE_SIZE: f32 = 20.0;

					// Bars
					let mut x = bb.x;
					let mid = bb.y + bb.h * 0.5;
					for idx in 0..notches.get() {
						let height = if idx == 0 || idx == notches.get() - 1 { 20.0 }
							else { 15.0 };

						hud_state.draw_rect(UIRect {
							x, y: mid - height * 0.5,
							w: BAR_WIDTH,
							h: height,
						}, SLIDER_BAR_COLOR, Corners::repeat(BAR_WIDTH * 0.5), hud::FillMode::Fill);
						x += notch_spacing;
					}

					// Handle
					hud_state.draw_rect(UIRect {
						x: value.delerp(*range.start(), *range.end())
							.lerp(bb.x - HANDLE_SIZE * 0.5, bb.x + bb.w - HANDLE_SIZE * 0.5),
						y: mid - HANDLE_SIZE * 0.5,
						w: HANDLE_SIZE,
						h: HANDLE_SIZE,
					}, if menu.item_selected != index { SLIDER_HANDLE_COLOR } else { SLIDER_HIGHLIGHTED_COLOR },
					Corners::repeat(HANDLE_SIZE * 0.5), hud::FillMode::Fill);

					// Label
					let (lw, lh) = (TextState::buffer_width(label_buffer), label_buffer.metrics().line_height);
					text_state.push_area(gp::TextArea {
						buffer: label_buffer,
						left: bb.x + bb.w * 0.5 - lw * 0.5,
						top: ty + bb.h * 0.5 - lh * 0.5,
						scale: 1.0,
						bounds: gp::TextBounds::default(),
						default_color: SLIDER_LABEL_COLOR,
					});
				}
			}
		}
	}

	pub fn mouse_down(&mut self, text_state: &TextState) {
		if self.commited_action.is_some() {
			return;
		}

		let Some(mut menu) = self.stack.last() else { return };
		menu = unsafe { crate::util::reborrow(menu) };

		match menu.layout.get(menu.item_selected as usize) {
			Some(&MenuLayoutItem::Button { addr, ref kind, .. }) => {
				self.commited_action = Some(CommitedAction::Navigation);
				let actions = MenuActions(self, text_state);
				match (kind, &menu.menu.items[addr as usize].kind) {
					(
						&LayoutButtonKind::Regular { row_index, .. },
						MenuItemKind::ButtonRow { buttons, .. },
					) => {
						let button = &buttons[row_index as usize];
						(button.action)(actions);
					}
					(
						LayoutButtonKind::KeyBind{..},
						MenuItemKind::KeyBinding{..},
					) => todo!(),

					_ => unreachable!(),
				}
			}
			Some(MenuLayoutItem::IntSlider { .. }) => {
				self.item_mouse_held = true;
				self.need_research = true;
			}
			_ => {}
		}
	}

	pub fn mouse_up(&mut self) {
		self.item_mouse_held = false;
	}

	fn push_entry<'m>(stack: &'m mut Vec<OpenMenu>, menu: &'m Menu, title_bar: &mut Buffer, text_state: &TextState) {
		stack.push(OpenMenu {
			menu: unsafe { crate::util::reborrow(menu) },
			item_selected: !0,
			button_selected: 0,
			layout: lay_out_menu(menu, text_state).into_boxed_slice(),
		});
		title_bar.lines.clear();
		title_bar.lines.push(menu.title.build_buffer_line().align(ct::Align::Center).0);
		title_bar.shape_until_scroll(&mut *text_state.font_system.borrow_mut());
	}

	fn pop_entry(&mut self, text_state: &TextState) {
		/*
		//self.stack.pop().expect("Menu stack cannot be empty");
		if let [.., ref menu, _] = *self.stack {
			self.title_bar.lines.clear();
			self.title_bar.lines.push(menu.menu.title.build_buffer_line().align(ct::Align::Center).0);
			self.title_bar.shape_until_scroll(&mut *text_state.font_system.borrow_mut());
			self.stack.pop();
		} else {
			self.close();
		}
		*/

		self.item_mouse_held = false;
		self.stack.pop().expect("Menu stack cannot be empty");
		if let Some(menu) = self.stack.last() {
			self.title_bar.lines.clear();
			self.title_bar.lines.push(menu.menu.title.build_buffer_line().align(ct::Align::Center).0);
			self.title_bar.shape_until_scroll(&mut *text_state.font_system.borrow_mut());
		} else {
			self.animation_state = OpeningMenuState::Closed;
		}
	}

	fn submit_event(&self, event: crate::AppEvent) {
		let _ = self.event_proxy.send_event(event);
	}
}

fn lay_out_menu(menu: &Menu, text_state: &TextState) -> Vec<MenuLayoutItem> {
	let mut layout = Vec::new();
	let mut current_y = 100.0;
	for (item, item_id) in menu.items.iter().zip(0u32..) {
		let mut current_x = 30.0;
		if let Some(ref title) = item.title {
			layout.push(MenuLayoutItem::Label {
				position: UIPoint {
					x: current_x,
					y: current_y,
				},
				buffer: text_state.create_buffer_line(20.0, 20.0, title),
			});
			current_x += 300.0;
		}
		match item.kind {
			MenuItemKind::Label => {}
			MenuItemKind::FloatSlider {..} => {}
			MenuItemKind::IntSlider { ref range, increment, ref target, .. } => {
				let span = u32::try_from(range.end() - range.start()).unwrap();
				let width = 250.0;
				let notches = unsafe { std::num::NonZeroU32::new_unchecked(1 + span / increment.get()) };
				let notch_spacing = if span == 0 { 0.0 } else { width / span as f32 * increment.get() as f32 };
				let (value, display) = target.get_current();
				let display = display.unwrap_or_else(|| value.to_string());
				let label_buffer = text_state.create_buffer_line(20.0, 16.0, &TextLine::new(display, AttrsPreset::Slider));
				layout.push(MenuLayoutItem::IntSlider {
					bb: UIRect {
						x: current_x,
						y: current_y,
						w: width,
						h: 22.0,
					},
					addr: item_id,
					value,
					//display,
					label_buffer,
					notch_spacing,
					notches,
				});
			}
			MenuItemKind::KeyBinding {..} => {
				layout.push(MenuLayoutItem::Button {
					bb: UIRect {
						x: current_x,
						y: current_y,
						w: 200.0,
						h: 22.0,
					},
					addr: item_id,
					kind: LayoutButtonKind::KeyBind {
						label: None,
					},
				});
			}
			MenuItemKind::ButtonRow { ref buttons } => {
				for (button, button_id) in buttons.iter().zip(0u32..) {
					let label = text_state.create_buffer_line(20.0, 20.0, &button.title);
					let width = TextState::buffer_width(&label) + 10.0;
					layout.push(MenuLayoutItem::Button {
						bb: UIRect {
							x: current_x,
							y: current_y,
							w: width,
							h: 22.0,
						},
						addr: item_id,
						kind: LayoutButtonKind::Regular {
							row_index: button_id,
							label,
						},
					});
					current_x += width + 10.0;
				}
			}
		}
		current_y += 30.0;
	}

	if let Ok(max) = usize::try_from(u32::MAX) {
		assert!(layout.len() < max, "Layout item count exceeds the logical limit");
	}

	layout
}

pub struct MenuActions<'l, 't>(&'l mut OpenMenuStack, &'t TextState);

impl<'l> MenuActions<'l, '_> {
	#[inline]
	pub fn pop_entry(self) {
		self.0.pop_entry(self.1);
	}

	#[inline]
	pub fn push_entry(self, menu: &'l Menu) {
		OpenMenuStack::push_entry(&mut self.0.stack, menu, &mut self.0.title_bar, self.1);
	}

	#[inline]
	pub fn change_gamemode(self, mode: crate::GameMode) {
		self.0.commited_action = Some(CommitedAction::GameMode(mode));
	}

	#[inline]
	pub(crate) fn submit_event(&self, event: crate::AppEvent) {
		self.0.submit_event(event);
	}
}

impl Menu {
	pub fn new(title: TextLine) -> Box<Self> {
		Box::new(Menu {
			title,
			items: Vec::new(),
		})
	}
}

#[doc(hidden)]
#[inline]
pub fn __menu_from_string(str: &'static str) -> Box<Menu> {
	// const ATTRS: ct::Attrs = ;
	Menu::new(TextLine::new(str, AttrsPreset::Title))
}

pub macro build_menu(
	$name:literal ;
	$($menu_item:tt)*
) {{
	let mut menu = crate::menu::__menu_from_string($name);
	crate::menu::__build_menu_items!(menu; $($menu_item)*);
	::std::rc::Rc::from(menu)
}}

pub macro build_submenu(
	$name:literal ;
	$($menu_item:tt)*
) {{
	let mut menu = crate::menu::__menu_from_string($name);
	crate::menu::__build_menu_items!(menu; $($menu_item)*);
	menu
}}

#[doc(hidden)]
pub macro __build_menu_items {
	// Empty
	($menu:ident;) => (),
	// Back button
	{
		$menu:ident;
		-$label:literal;
		$($rest:tt)*
	} => {
		$menu.items.push(crate::menu::__menu_back_button($label));
		crate::menu::__build_menu_items!($menu; $($rest)*);
	},
	// Action button
	{
		$menu:ident;
		!$label:literal => |$actions:pat_param| $action:block
		$($rest:tt)*
	} => {
		$menu.items.push(__menu_item(None, MenuItemKind::ButtonRow {
			buttons: vec![crate::menu::__menu_action_button($label, Box::new(move |$actions| $action))],
		}));
		crate::menu::__build_menu_items!($menu; $($rest)*);
	},
	// Buttons row
	{
		$menu:ident;
		!$($label:literal)? {
			$($btn_label:literal => |$actions:pat_param| $action:block)*
		}
		$($rest:tt)*
	} => {
		$menu.items.push(__menu_item(crate::menu::__option!($($label)?), MenuItemKind::ButtonRow {
			buttons: vec![
				$(crate::menu::__menu_action_button($btn_label, Box::new(move |$actions| $action)),)*
			],
		}));
		crate::menu::__build_menu_items!($menu; $($rest)*);
	},
	// Submenu
	{
		$menu:ident;
		#$label:literal => {$($subm:tt)*}
		$($rest:tt)*
	} => {
		$menu.items.push(crate::menu::__menu_submenu($label, crate::menu::build_submenu!($($subm)*)));
		crate::menu::__build_menu_items!($menu; $($rest)*);
	},
	// Key binding
	{
		$menu:ident;
		&$label:literal = $binding:ident;
		$($rest:tt)*
	} => {
		$menu.items.push($crate::menu::__menu_keybind($label, cont::Button::$binding));
		crate::menu::__build_menu_items!($menu; $($rest)*);
	},
	// Custom labeled entry
	{
		$menu:ident;
		@$label:literal => $entry:expr;
		$($rest:tt)*
	} => {
		$menu.items.push($crate::menu::__menu_item(Some($label), $entry));
		crate::menu::__build_menu_items!($menu; $($rest)*);
	},
}

#[doc(hidden)]
pub macro __option {
	() => (None),
	($expr:expr) => (Some($expr)),
}

#[doc(hidden)]
#[inline]
pub fn __menu_item(label: Option<&'static str>, kind: MenuItemKind) -> MenuItem {
	MenuItem {
		title: label.map(|label| TextLine::new(label, AttrsPreset::Item)),
		kind,
	}
}

#[doc(hidden)]
#[inline]
pub fn __menu_back_button(label: &'static str) -> MenuItem {
	__menu_item(None, MenuItemKind::ButtonRow {
		buttons: vec![ButtonItem {
			title: TextLine::new(label, AttrsPreset::BackButton),
			action: Box::new(ButtonItem::action_back),
		}],
	})
}

#[doc(hidden)]
#[inline]
pub fn __menu_action_button(label: &'static str, action: Box<ButtonAction>) -> ButtonItem {
	ButtonItem {
		title: TextLine::new(label, AttrsPreset::Button),
		action,
	}
}

#[doc(hidden)]
#[inline]
pub fn __menu_submenu(label: &'static str, menu: Box<Menu>) -> MenuItem {
	__menu_item(None, MenuItemKind::ButtonRow {
		buttons: vec![ButtonItem {
			title: TextLine::new(label, AttrsPreset::Button),
			action: Box::new(move |actions| {
				actions.push_entry(&menu);
			}),
		}],
	})
}

#[doc(hidden)]
#[inline]
pub fn __menu_keybind(label: &'static str, button: cont::Button) -> MenuItem {
	__menu_item(Some(label), MenuItemKind::KeyBinding {
		target: button,
	})
}

enum AttrsPreset {
	Title,
	Item,
	Button,
	BackButton,
	Slider,
}

impl From<AttrsPreset> for Attrs<'_> {
	fn from(preset: AttrsPreset) -> Self {
		match preset {
			AttrsPreset::Title => Attrs {
				color_opt: None,
				family: ct::Family::SansSerif,
				stretch: ct::Stretch::Normal,
				style: ct::Style::Normal,
				weight: ct::Weight::SEMIBOLD,
				metadata: 0,
			},
			AttrsPreset::Item => Attrs {
				color_opt: None,
				family: ct::Family::SansSerif,
				stretch: ct::Stretch::Normal,
				style: ct::Style::Normal,
				weight: ct::Weight::NORMAL,
				metadata: 0,
			},
			AttrsPreset::Button => Attrs {
				color_opt: None,
				family: ct::Family::SansSerif,
				stretch: ct::Stretch::Normal,
				style: ct::Style::Normal,
				weight: ct::Weight::NORMAL,
				metadata: 0,
			},
			AttrsPreset::BackButton => Attrs {
				color_opt: None,
				family: ct::Family::SansSerif,
				stretch: ct::Stretch::Normal,
				style: ct::Style::Normal,
				weight: ct::Weight::NORMAL,
				metadata: 0,
			},
			AttrsPreset::Slider => Attrs {
				color_opt: None,
				family: ct::Family::SansSerif,
				stretch: ct::Stretch::Normal,
				style: ct::Style::Normal,
				weight: ct::Weight::SEMIBOLD,
				metadata: 0,
			},
		}
	}
}
