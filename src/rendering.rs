use std::{mem::{size_of, offset_of, MaybeUninit}, rc::Rc};
use wgpu::util::DeviceExt;
use crate::{
	AnyResult,
	App,
	display::Display,
	shader_store::ShaderStore,
	util::SliceExt,
};

pub mod hud;
pub mod buffers;

use buffers::UpdatableBuffer;

/// An enum for depth & stencil format, such that the size stays small
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum DepthStencilFormat {
	/// Stencil format with 8 bit integer stencil.
	Stencil8,
	/// Special depth format with 16 bit integer depth.
	Depth16Unorm,
	/// Special depth format with at least 24 bit integer depth.
	Depth24Plus,
	/// Special depth/stencil format with at least 24 bit integer depth and 8 bits integer stencil.
	Depth24PlusStencil8,
	/// Special depth format with 32 bit floating point depth.
	Depth32Float,
	/// Special depth/stencil format with 32 bit floating point depth and 8 bits integer stencil.
	///
	/// [`Features::DEPTH32FLOAT_STENCIL8`] must be enabled to use this texture format.
	Depth32FloatStencil8,
}

impl DepthStencilFormat {
	#[inline]
	pub const fn to_format(self) -> wgpu::TextureFormat {
		use wgpu::TextureFormat;
		match self {
			Self::Stencil8 => TextureFormat::Stencil8,
			Self::Depth16Unorm => TextureFormat::Depth16Unorm,
			Self::Depth24Plus => TextureFormat::Depth24Plus,
			Self::Depth24PlusStencil8 => TextureFormat::Depth24PlusStencil8,
			Self::Depth32Float => TextureFormat::Depth32Float,
			Self::Depth32FloatStencil8 => TextureFormat::Depth32FloatStencil8,
		}
	}

	#[inline]
	pub const fn from_format(format: wgpu::TextureFormat) -> Option<Self> {
		use wgpu::TextureFormat;
		match format {
			TextureFormat::Stencil8 => Some(Self::Stencil8),
			TextureFormat::Depth16Unorm => Some(Self::Depth16Unorm),
			TextureFormat::Depth24Plus => Some(Self::Depth24Plus),
			TextureFormat::Depth24PlusStencil8 => Some(Self::Depth24PlusStencil8),
			TextureFormat::Depth32Float => Some(Self::Depth32Float),
			TextureFormat::Depth32FloatStencil8 => Some(Self::Depth32FloatStencil8),
			_ => None,
		}
	}
}

impl From<DepthStencilFormat> for wgpu::TextureFormat {
	#[inline(always)] fn from(format: DepthStencilFormat) -> Self { format.to_format() }
}

pub(crate) const fn buffer_size<T: Sized>() -> wgpu::BufferSize {
	// assert_ne!(size_of::<T>(), 0, "Type must not have 0 size");
	assert!(size_of::<T>() != 0, "Type must not have 0 size");
	unsafe {
		wgpu::BufferSize::new_unchecked(size_of::<T>() as u64)
	}
}

/// A texture with a view.
pub struct TextureAndView {
	pub texture: wgpu::Texture,
	pub view: wgpu::TextureView,
}

pub trait Pipeline {
	fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>);
}

pub(crate) fn create_depth_texture(device: &wgpu::Device, config: &wgpu::SurfaceConfiguration, format: DepthStencilFormat) -> TextureAndView {
	let descriptor = wgpu::TextureDescriptor {
		label: None,
		size: wgpu::Extent3d {
			width: config.width,
			height: config.height,
			depth_or_array_layers: 1,
		},
		mip_level_count: 1,
		sample_count: 1,
		dimension: wgpu::TextureDimension::D2,
		format: format.to_format(),
		usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
		view_formats: &[],
	};
	let texture = device.create_texture(&descriptor);

	let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
	/*let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
		label: None,
		address_mode_u: wgpu::AddressMode::ClampToEdge,
		address_mode_v: wgpu::AddressMode::ClampToEdge,
		address_mode_w: wgpu::AddressMode::ClampToEdge,
		mag_filter: wgpu::FilterMode::Linear,
		min_filter: wgpu::FilterMode::Linear,
		mipmap_filter: wgpu::FilterMode::Nearest,
		lod_min_clamp: 1.0,
		lod_max_clamp: 1.0,
		compare: Some(wgpu::CompareFunction::LessEqual),
		anisotropy_clamp: 1,
		border_color: None,
	});*/

	TextureAndView {
		texture,
		view,
	}
}

pub struct CommonPipelineData {
	/// Common static data, like projection matrices
	pub shared_shader_buffer: wgpu::Buffer,
	shared_shader_group_layout: wgpu::BindGroupLayout,
	shared_shader_group: wgpu::BindGroup,
}

#[repr(C)]
pub struct CommonShaderData {
	pub vp_matrix: cgmath::Matrix4<f32>,
}

pub struct SimplePipeline {
	shader: wgpu::ShaderModule,
	main_pipeline: wgpu::RenderPipeline,

	vertex_buffer: wgpu::Buffer,
	vertex_count: u32,
}

#[derive(Clone, Copy)]
#[repr(C)]
struct SimpleVertex {
	pos: [f32; 3],
	clr: [f32; 3],
}

pub struct PartPipelineCommon {
	shader_cube: wgpu::ShaderModule,
	shader_wireframe: wgpu::ShaderModule,
	layout: wgpu::PipelineLayout,
	/// Static buffer with cube indices
	part_index_buffer: wgpu::Buffer,
	common: Rc<CommonPipelineData>,
}

pub struct PartPipeline {
	fill: wgpu::RenderPipeline,
	wire: wgpu::RenderPipeline,

	shared: Rc<PartPipelineCommon>,
}

pub struct BackgroundPipeline {
	shader: wgpu::ShaderModule,
	pipeline: wgpu::RenderPipeline,
}

const PART_FILL_INDICES: u32 = 6 * 6;
const PART_WIRE_INDICES: u32 = 8 * 3;
const PART_WIRE2_INDICES: u32 = 3 * 8 * 3;

impl CommonPipelineData {
	pub fn create(device: &wgpu::Device) -> Rc<Self> {
		let shared_shader_buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label: Some("Common shader data"),
			size: size_of::<CommonShaderData>() as wgpu::BufferAddress,
			usage: wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
			mapped_at_creation: false,
		});

		let shared_shader_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
			label: None,
			entries: &[wgpu::BindGroupLayoutEntry {
				binding: 0,
				visibility: wgpu::ShaderStages::VERTEX,
				ty: wgpu::BindingType::Buffer {
					ty: wgpu::BufferBindingType::Storage { read_only: true },
					has_dynamic_offset: false,
					min_binding_size: Some(buffer_size::<[[f32; 4]; 4]>()),
				},
				count: None,
			}],
		});

		let shared_shader_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
			label: None,
			layout: &shared_shader_group_layout,
			entries: &[wgpu::BindGroupEntry {
				binding: 0,
				resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
					buffer: &shared_shader_buffer,
					offset: 0,
					size: Some(buffer_size::<CommonShaderData>()),
				}),
			}],
		});

		Rc::new(CommonPipelineData {
			shared_shader_buffer,
			shared_shader_group_layout,
			shared_shader_group,
		})
	}
}

impl SimplePipeline {
	pub fn create(display: &Display, shader_store: &ShaderStore) -> AnyResult<Box<Self>> {
		#[allow(unused_variables)]
		let [mod_hello, mod_mandelbrot] = Self::load_shaders(&display.device, shader_store)?;
		let device = &display.device;

		#[allow(non_upper_case_globals)]
		static pld: wgpu::PipelineLayoutDescriptor = wgpu::PipelineLayoutDescriptor {
			label: Some("Simple render pipeline layout"),
			bind_group_layouts: &[],
			push_constant_ranges: &[],
		};
		let layout = device.create_pipeline_layout(&pld);

		let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
			label: Some("Simple render pipeline"),
			layout: Some(&layout),
			vertex: wgpu::VertexState {
				module: &mod_hello,
				entry_point: "vs_main",
				buffers: &[wgpu::VertexBufferLayout {
					array_stride: size_of::<SimpleVertex>() as wgpu::BufferAddress,
					step_mode: wgpu::VertexStepMode::Vertex,
					attributes: &[
						// Position
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x3,
							offset: offset_of!(SimpleVertex, pos) as wgpu::BufferAddress,
							shader_location: 0,
						},
						// Color
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x3,
							offset: offset_of!(SimpleVertex, clr) as wgpu::BufferAddress,
							shader_location: 1,
						},
					],
				}],
			},
			primitive: wgpu::PrimitiveState {
				topology: wgpu::PrimitiveTopology::TriangleStrip,
				strip_index_format: None,
				front_face: wgpu::FrontFace::Ccw,
				// For test
				cull_mode: Some(wgpu::Face::Back),
				unclipped_depth: false,
				polygon_mode: wgpu::PolygonMode::Fill,
				conservative: false,
			},
			depth_stencil: display.depth_stencil_state_disabled(),
			multisample: wgpu::MultisampleState::default(),
			fragment: Some(wgpu::FragmentState {
				module: &mod_hello,
				entry_point: "fs_main",
				targets: &[Some(wgpu::ColorTargetState {
					format: display.config.format,
					blend: Some(wgpu::BlendState::REPLACE),
					write_mask: wgpu::ColorWrites::ALL,
				})],
			}),
			multiview: None,
		});

		#[allow(non_upper_case_globals)]
		const vertices: &[SimpleVertex] = &[
			SimpleVertex { pos: [-0.3, -0.3, 0.0], clr: [0.52, 0.57, 0.73] },
			SimpleVertex { pos: [ 0.3, -0.3, 0.0], clr: [0.9,  0.4,  0.5 ] },
			SimpleVertex { pos: [-0.3,  0.3, 0.0], clr: [0.21, 0.44, 0.95] },
			SimpleVertex { pos: [ 0.3,  0.3, 0.0], clr: [0.46, 0.85, 0.2 ] },
			SimpleVertex { pos: [ 0.0,  0.5, 0.0], clr: [0.62, 0.32, 0.57] },
		];

		let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
			label: None,
			contents: vertices.as_bytes(),
			usage: wgpu::BufferUsages::VERTEX,
		});

		let vertex_count = vertices.len() as u32;

		Ok(Box::new(SimplePipeline {
			shader: mod_hello,
			main_pipeline: pipeline,
			vertex_buffer,
			vertex_count,
		}))
	}

	fn load_shaders(device: &wgpu::Device, shader_store: &ShaderStore) -> AnyResult<[wgpu::ShaderModule; 2]> {
		let src_hello = shader_store.load_shader_src("hello")?;
		let src_mandelbrot = shader_store.load_shader_src("mandelbrot")?;

		let mod_hello = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("Hello shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_hello)),
		});

		let mod_mandelbrot = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("Mandelbrot render shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_mandelbrot)),
		});

		Ok([mod_hello, mod_mandelbrot])
	}
}

impl Pipeline for SimplePipeline {
	fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>) {
		render_pass.set_pipeline(&self.main_pipeline);
		render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
		render_pass.draw(0..self.vertex_count, 0..1);
	}
}

impl PartPipelineCommon {
	pub(crate) fn new(app: &App) -> AnyResult<Rc<Self>> {
		let [shader_cube, shader_wireframe] = PartPipeline::load_shaders(&app.display.device, &app.shader_store)?;
		let common = app.cached_objects.common_pipeline_data.clone();
		let device = &app.display.device;

		let pld = wgpu::PipelineLayoutDescriptor {
			label: Some("Part pipeline layout"),
			bind_group_layouts: &[&common.shared_shader_group_layout],
			push_constant_ranges: &[],
		};
		let layout = device.create_pipeline_layout(&pld);

		#[allow(non_upper_case_globals)]
		const part_indices: &[u16] = &[
			// Fill indices, 3 per primitive; 6 per side
			0, 2, 1,
			1, 2, 3,
			4, 5, 6,
			6, 5, 7,

			0, 1, 4,
			4, 1, 5,
			2, 6, 3,
			3, 6, 7,

			1, 3, 5,
			5, 3, 7,
			0, 4, 2,
			2, 4, 6,

			// Outline indices, 2 per primitive; 8 per "level"
			4, 5, 5, 7, 7, 6, 6, 4,
			4, 0, 1, 5, 7, 3, 2, 6,
			0, 2, 2, 3, 3, 1, 1, 0,

			// Outline indices for wireframe fill mode.
			// 3 per primitive; 24 per "level"
			0x0, 0x1, 0x8, 0x8, 0x1, 0x9,
			0x1, 0x5, 0x9, 0x9, 0x5, 0xD,
			0x5, 0x4, 0xD, 0xD, 0x4, 0xC,
			0x4, 0x0, 0xC, 0xC, 0x0, 0x8,

			0xA, 0xB, 0x2, 0x2, 0xB, 0x3,
			0xB, 0xF, 0x3, 0x3, 0xF, 0x7,
			0xF, 0xE, 0x7, 0x7, 0xE, 0x6,
			0xE, 0xA, 0x6, 0x6, 0xA, 0x2,

			0x2, 0x0, 0xA, 0xA, 0x0, 0x8,
			0xB, 0x9, 0x3, 0x3, 0x9, 0x1,
			0xF, 0xD, 0x7, 0x7, 0xD, 0x5,
			0x6, 0x4, 0xE, 0xE, 0x4, 0xC,
		];

		let part_index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
			label: None,
			contents: part_indices.as_bytes(),
			usage: wgpu::BufferUsages::INDEX,
		});

		const _: () = assert!(part_indices.len() == PART_FILL_INDICES as usize + PART_WIRE_INDICES as usize + PART_WIRE2_INDICES as usize);

		Ok(Rc::new(PartPipelineCommon {
			shader_cube,
			shader_wireframe,
			layout,
			part_index_buffer,
			common,
		}))
	}

	const INDEX_OFFSET_FILL: wgpu::BufferAddress = 0;
	const INDEX_OFFSET_WIRE: wgpu::BufferAddress = Self::INDEX_OFFSET_FILL + PART_FILL_INDICES as wgpu::BufferAddress * 2;
	const INDEX_OFFSET_WIRE2: wgpu::BufferAddress = Self::INDEX_OFFSET_WIRE + PART_WIRE_INDICES as wgpu::BufferAddress * 2;
	const INDEX_OFFSET_END: wgpu::BufferAddress = Self::INDEX_OFFSET_WIRE2 + PART_WIRE2_INDICES as wgpu::BufferAddress * 2;

	fn part_fill_index(&self) -> wgpu::BufferSlice {
		self.part_index_buffer.slice(Self::INDEX_OFFSET_FILL..Self::INDEX_OFFSET_WIRE)
	}

	fn part_wire_index(&self) -> wgpu::BufferSlice {
		self.part_index_buffer.slice(Self::INDEX_OFFSET_WIRE..Self::INDEX_OFFSET_WIRE2)
	}

	fn part_wire2_index(&self) -> wgpu::BufferSlice {
		self.part_index_buffer.slice(Self::INDEX_OFFSET_WIRE2..Self::INDEX_OFFSET_END)
	}
}

impl PartPipeline {
	pub const VERTEX_BUFFER_USAGES: wgpu::BufferUsages = UpdatableBuffer::REQUIRED_BUFFER_USAGES.union(wgpu::BufferUsages::VERTEX);

	pub fn create(display: &Display, common: Rc<PartPipelineCommon>) -> AnyResult<Rc<Self>> {
		use crate::level::part::PartInstance;

		let module = &common.shader_cube;
		let device = &display.device;

		let targets = [Some(wgpu::ColorTargetState {
			format: display.config.format,
			blend: Some(wgpu::BlendState::REPLACE),
			write_mask: wgpu::ColorWrites::ALL,
		})];

		let mut pipeline_descriptor = wgpu::RenderPipelineDescriptor {
			label: Some("part-cube pipeline"),
			layout: Some(&common.layout),
			vertex: wgpu::VertexState {
				module,
				entry_point: "vs_main",
				buffers: &[wgpu::VertexBufferLayout {
					array_stride: size_of::<PartInstance>() as wgpu::BufferAddress,
					step_mode: wgpu::VertexStepMode::Instance,
					attributes: &[
						// Origin
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x3,
							offset: offset_of!(PartInstance, origin) as wgpu::BufferAddress,
							shader_location: 0,
						},
						// Extent
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x3,
							offset: offset_of!(PartInstance, extent) as wgpu::BufferAddress,
							shader_location: 1,
						},
					],
				}],
			},
			primitive: wgpu::PrimitiveState {
				topology: wgpu::PrimitiveTopology::TriangleList,
				strip_index_format: None,
				front_face: wgpu::FrontFace::Ccw,
				cull_mode: Some(wgpu::Face::Back),
				unclipped_depth: false,
				polygon_mode: wgpu::PolygonMode::Fill,
				conservative: false,
			},
			depth_stencil: Some(wgpu::DepthStencilState {
				format: display.depth_stencil_format.to_format(),
				depth_write_enabled: true,
				depth_compare: wgpu::CompareFunction::LessEqual,
				stencil: wgpu::StencilState::default(),
				bias: wgpu::DepthBiasState::default(),
			}),
			multisample: wgpu::MultisampleState::default(),
			fragment: Some(wgpu::FragmentState {
				module,
				entry_point: "fs_fill",
				targets: &targets,
			}),
			multiview: None,
		};

		let fill_pipeline = device.create_render_pipeline(&pipeline_descriptor);
		pipeline_descriptor.primitive.polygon_mode = wgpu::PolygonMode::Fill;
		pipeline_descriptor.primitive.topology = wgpu::PrimitiveTopology::LineList;
		unsafe { pipeline_descriptor.fragment.as_mut().unwrap_unchecked() }.entry_point = "fs_outline";
		if let Some(ref mut depth_stencil) = pipeline_descriptor.depth_stencil {
			depth_stencil.depth_write_enabled = false;
		}
		let wire_pipeline = device.create_render_pipeline(&pipeline_descriptor);

		Ok(Rc::new(PartPipeline {
			fill: fill_pipeline,
			wire: wire_pipeline,
			shared: common,
		}))
	}

	fn load_shaders(device: &wgpu::Device, shader_store: &ShaderStore) -> AnyResult<[wgpu::ShaderModule; 2]> {
		let src_part_cube = shader_store.load_shader_src("part-cube")?;

		let mod_part_cube = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("part-cube shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_part_cube)),
		});

		let src_part_wireframe = shader_store.load_shader_src("part-wireframe")?;

		let mod_part_wireframe = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("part-wireframe shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_part_wireframe)),
		});

		Ok([mod_part_cube, mod_part_wireframe])
	}

	pub fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>, part_buffer: wgpu::BufferSlice<'r>, part_count: u32) {
		render_pass.set_pipeline(&self.fill);
		render_pass.set_bind_group(0, &self.shared.common.shared_shader_group, &[]);
		render_pass.set_index_buffer(self.shared.part_fill_index(), wgpu::IndexFormat::Uint16);
		render_pass.set_vertex_buffer(0, part_buffer);
		render_pass.draw_indexed(0..PART_FILL_INDICES, 0, 0..part_count);

		if false {
			render_pass.set_pipeline(&self.wire);
			render_pass.set_index_buffer(self.shared.part_wire_index(), wgpu::IndexFormat::Uint16);
			render_pass.set_vertex_buffer(0, part_buffer);
			render_pass.draw_indexed(0..PART_WIRE_INDICES, 0, 0..part_count);
		}
	}
}

impl BackgroundPipeline {
	pub fn create(display: &Display, shader_store: &ShaderStore) -> AnyResult<Box<Self>> {
		let shader = Self::load_shaders(&display.device, shader_store)?;
		let device = &display.device;

		#[allow(non_upper_case_globals)]
		static pld: wgpu::PipelineLayoutDescriptor = wgpu::PipelineLayoutDescriptor {
			label: Some("Background pipeline layout"),
			bind_group_layouts: &[],
			push_constant_ranges: &[],
		};
		let layout = device.create_pipeline_layout(&pld);

		let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
			label: Some("Background render pipeline"),
			layout: Some(&layout),
			vertex: wgpu::VertexState {
				module: &shader,
				entry_point: "vs_main",
				buffers: &[],
			},
			primitive: wgpu::PrimitiveState {
				topology: wgpu::PrimitiveTopology::TriangleStrip,
				strip_index_format: None,
				front_face: wgpu::FrontFace::Ccw,
				cull_mode: None,
				unclipped_depth: false,
				polygon_mode: wgpu::PolygonMode::Fill,
				conservative: false,
			},
			depth_stencil: display.depth_stencil_state_disabled(),
			multisample: wgpu::MultisampleState::default(),
			fragment: Some(wgpu::FragmentState {
				module: &shader,
				entry_point: "fs_main",
				targets: &[Some(wgpu::ColorTargetState {
					format: display.config.format,
					blend: Some(wgpu::BlendState::REPLACE),
					write_mask: wgpu::ColorWrites::ALL,
				})],
			}),
			multiview: None,
		});

		Ok(Box::new(BackgroundPipeline {
			shader,
			pipeline,
		}))
	}

	fn load_shaders(device: &wgpu::Device, shader_store: &ShaderStore) -> AnyResult<wgpu::ShaderModule> {
		let src_simple = shader_store.load_shader_src("background-simple")?;

		let mod_simple = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("background-simple shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_simple)),
		});

		Ok(mod_simple)
	}
}

impl Pipeline for BackgroundPipeline {
	fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>) {
		render_pass.set_pipeline(&self.pipeline);
		render_pass.draw(0..4, 0..1);
	}
}

pub struct PartPlacerGhostPipeline {
	fill_pipeline: wgpu::RenderPipeline,
	wire_pipeline: wgpu::RenderPipeline,
	shader: wgpu::ShaderModule,
	pub buffer: wgpu::Buffer,
	bind_group: wgpu::BindGroup,
	part_common: Rc<PartPipelineCommon>,
}

#[repr(C)]
pub struct PartGhostAttributes {
	pub origin: [f32; 3],
	pub _pad: MaybeUninit<[f32; 1]>,
	pub extent: [f32; 3],
	pub opacity: f32,
}

impl PartPlacerGhostPipeline {
	pub fn new(app: &App) -> AnyResult<Self> {
		let part_common = app.cached_objects().part_common();

		let device = &app.display.device;
		let shader = Self::load_shaders(&app.display.device, &app.shader_store)?;
		let module = &shader;

		let attribute_buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label: None,
			size: size_of::<PartGhostAttributes>() as wgpu::BufferAddress,
			usage: wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
			mapped_at_creation: false,
		});

		let group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
			label: None,
			entries: &[wgpu::BindGroupLayoutEntry {
				binding: 0,
				visibility: wgpu::ShaderStages::VERTEX_FRAGMENT,
				ty: wgpu::BindingType::Buffer {
					ty: wgpu::BufferBindingType::Storage { read_only: true },
					has_dynamic_offset: false,
					min_binding_size: Some(buffer_size::<PartGhostAttributes>()),
				},
				count: None,
			}],
		});

		let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
			label: None,
			layout: &group_layout,
			entries: &[wgpu::BindGroupEntry {
				binding: 0,
				resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
					buffer: &attribute_buffer,
					offset: 0,
					size: Some(buffer_size::<PartGhostAttributes>()),
				}),
			}],
		});

		let targets = [Some(wgpu::ColorTargetState {
			format: app.display.config.format,
			blend: Some(wgpu::BlendState::ALPHA_BLENDING),
			write_mask: wgpu::ColorWrites::ALL,
		})];

		let pld = wgpu::PipelineLayoutDescriptor {
			label: Some("Part ghost pipeline layout"),
			bind_group_layouts: &[&part_common.common.shared_shader_group_layout, &group_layout],
			push_constant_ranges: &[],
		};
		let layout = device.create_pipeline_layout(&pld);

		let mut pipeline_descriptor = wgpu::RenderPipelineDescriptor {
			label: Some("part-ghost fill pipeline"),
			layout: Some(&layout),
			vertex: wgpu::VertexState {
				module,
				entry_point: "vs_main",
				buffers: &[],
			},
			primitive: wgpu::PrimitiveState {
				topology: wgpu::PrimitiveTopology::TriangleList,
				strip_index_format: None,
				front_face: wgpu::FrontFace::Ccw,
				cull_mode: Some(wgpu::Face::Back),
				unclipped_depth: false,
				polygon_mode: wgpu::PolygonMode::Fill,
				conservative: false,
			},
			depth_stencil: Some(wgpu::DepthStencilState {
				format: app.display.depth_stencil_format.to_format(),
				depth_write_enabled: false,
				depth_compare: wgpu::CompareFunction::LessEqual,
				stencil: wgpu::StencilState::default(),
				bias: wgpu::DepthBiasState::default(),
			}),
			multisample: wgpu::MultisampleState::default(),
			fragment: Some(wgpu::FragmentState {
				module,
				entry_point: "fs_fill",
				targets: &targets,
			}),
			multiview: None,
		};

		let fill_pipeline = device.create_render_pipeline(&pipeline_descriptor);
		pipeline_descriptor.label = Some("part-ghost wire pipeline");
		pipeline_descriptor.primitive.polygon_mode = wgpu::PolygonMode::Fill;
		pipeline_descriptor.primitive.topology = wgpu::PrimitiveTopology::LineList;
		unsafe { pipeline_descriptor.fragment.as_mut().unwrap_unchecked() }.entry_point = "fs_outline";
		if let Some(ref mut depth_stencil) = pipeline_descriptor.depth_stencil {
			depth_stencil.depth_write_enabled = false;
		}
		let wire_pipeline = device.create_render_pipeline(&pipeline_descriptor);

		Ok(PartPlacerGhostPipeline {
			fill_pipeline,
			wire_pipeline,
			shader,
			buffer: attribute_buffer,
			bind_group,
			part_common,
		})
	}

	fn load_shaders(device: &wgpu::Device, shader_store: &ShaderStore) -> AnyResult<wgpu::ShaderModule> {
		let src_part_cube = shader_store.load_shader_src("ghost-cube")?;

		let mod_part_cube = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("ghost-cube shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_part_cube)),
		});

		Ok(mod_part_cube)
	}
}

impl Pipeline for PartPlacerGhostPipeline {
	fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>) {
		render_pass.set_pipeline(&self.fill_pipeline);
		render_pass.set_bind_group(0, &self.part_common.common.shared_shader_group, &[]);
		render_pass.set_bind_group(1, &self.bind_group, &[]);
		render_pass.set_index_buffer(self.part_common.part_fill_index(), wgpu::IndexFormat::Uint16);
		render_pass.draw_indexed(0..PART_FILL_INDICES, 0, 0..1);

		render_pass.set_pipeline(&self.wire_pipeline);
		render_pass.set_index_buffer(self.part_common.part_wire_index(), wgpu::IndexFormat::Uint16);
		render_pass.draw_indexed(0..PART_WIRE_INDICES, 0, 0..1);
	}
}

pub struct PartHighlightPipeline {
	pipeline: wgpu::RenderPipeline,
	shader: wgpu::ShaderModule,
	pub buffer: wgpu::Buffer,
	bind_group: wgpu::BindGroup,
	part_common: Rc<PartPipelineCommon>,
}

#[repr(C)]
pub struct PartHighlightAttributes {
	pub origin: [f32; 3],
	pub _pad: MaybeUninit<[f32; 1]>,
	pub extent: [f32; 3],
	pub color: u32,
}

impl PartHighlightPipeline {
	pub fn new(app: &App) -> AnyResult<Self> {
		let part_common = app.cached_objects().part_common();

		let device = &app.display.device;
		let shader = Self::load_shaders(&app.display.device, &app.shader_store)?;
		let module = &shader;

		let attribute_buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label: None,
			size: size_of::<PartHighlightAttributes>() as wgpu::BufferAddress,
			usage: wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
			mapped_at_creation: false,
		});

		let group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
			label: None,
			entries: &[wgpu::BindGroupLayoutEntry {
				binding: 0,
				visibility: wgpu::ShaderStages::VERTEX_FRAGMENT,
				ty: wgpu::BindingType::Buffer {
					ty: wgpu::BufferBindingType::Storage { read_only: true },
					has_dynamic_offset: false,
					min_binding_size: Some(buffer_size::<PartHighlightAttributes>()),
				},
				count: None,
			}],
		});

		let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
			label: None,
			layout: &group_layout,
			entries: &[wgpu::BindGroupEntry {
				binding: 0,
				resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
					buffer: &attribute_buffer,
					offset: 0,
					size: Some(buffer_size::<PartHighlightAttributes>()),
				}),
			}],
		});

		let targets = [Some(wgpu::ColorTargetState {
			format: app.display.config.format,
			blend: Some(wgpu::BlendState::ALPHA_BLENDING),
			write_mask: wgpu::ColorWrites::ALL,
		})];

		let pld = wgpu::PipelineLayoutDescriptor {
			label: Some("Part highlight pipeline layout"),
			bind_group_layouts: &[&part_common.common.shared_shader_group_layout, &group_layout],
			push_constant_ranges: &[],
		};
		let layout = device.create_pipeline_layout(&pld);

		let pipeline_descriptor = wgpu::RenderPipelineDescriptor {
			label: Some("part-highlight pipeline"),
			layout: Some(&layout),
			vertex: wgpu::VertexState {
				module,
				entry_point: "vs_main",
				buffers: &[],
			},
			primitive: wgpu::PrimitiveState {
				topology: wgpu::PrimitiveTopology::LineList,
				strip_index_format: None,
				front_face: wgpu::FrontFace::Ccw,
				cull_mode: Some(wgpu::Face::Back),
				unclipped_depth: false,
				polygon_mode: wgpu::PolygonMode::Fill,
				conservative: false,
			},
			depth_stencil: app.display.depth_stencil_state_disabled(),
			multisample: wgpu::MultisampleState::default(),
			fragment: Some(wgpu::FragmentState {
				module,
				entry_point: "fs_main",
				targets: &targets,
			}),
			multiview: None,
		};

		let pipeline = device.create_render_pipeline(&pipeline_descriptor);

		Ok(PartHighlightPipeline {
			pipeline,
			shader,
			buffer: attribute_buffer,
			bind_group,
			part_common,
		})
	}

	fn load_shaders(device: &wgpu::Device, shader_store: &ShaderStore) -> AnyResult<wgpu::ShaderModule> {
		let src_part_cube = shader_store.load_shader_src("highlight-cube")?;

		let mod_part_cube = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("highlight-cube shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_part_cube)),
		});

		Ok(mod_part_cube)
	}
}

impl Pipeline for PartHighlightPipeline {
	fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>) {
		render_pass.set_pipeline(&self.pipeline);
		render_pass.set_bind_group(0, &self.part_common.common.shared_shader_group, &[]);
		render_pass.set_bind_group(1, &self.bind_group, &[]);
		render_pass.set_index_buffer(self.part_common.part_wire_index(), wgpu::IndexFormat::Uint16);
		render_pass.draw_indexed(0..PART_WIRE_INDICES, 0, 0..1);
	}
}

/// Common pipeline for rendering parts' wireframes.
pub struct PartWireframePipeline {
	pipeline: wgpu::RenderPipeline,
	shared: Rc<PartPipelineCommon>,
}

impl PartWireframePipeline {
	pub const VERTEX_BUFFER_USAGES: wgpu::BufferUsages = UpdatableBuffer::REQUIRED_BUFFER_USAGES.union(wgpu::BufferUsages::VERTEX);

	pub fn create(display: &Display, common: Rc<PartPipelineCommon>) -> AnyResult<Rc<Self>> {
		use crate::level::part::PartInstance;

		let module = &common.shader_wireframe;
		let device = &display.device;

		let targets = [Some(wgpu::ColorTargetState {
			format: display.config.format,
			blend: Some(wgpu::BlendState::REPLACE),
			write_mask: wgpu::ColorWrites::ALL,
		})];

		let pipeline_descriptor = wgpu::RenderPipelineDescriptor {
			label: Some("part-wireframe pipeline"),
			layout: Some(&common.layout),
			vertex: wgpu::VertexState {
				module,
				entry_point: "vs_main",
				buffers: &[wgpu::VertexBufferLayout {
					array_stride: size_of::<PartInstance>() as wgpu::BufferAddress,
					step_mode: wgpu::VertexStepMode::Instance,
					attributes: &[
						// Origin
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x3,
							offset: offset_of!(PartInstance, origin) as wgpu::BufferAddress,
							shader_location: 0,
						},
						// Extent
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x3,
							offset: offset_of!(PartInstance, extent) as wgpu::BufferAddress,
							shader_location: 1,
						},
						// Color
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Unorm8x4,
							offset: offset_of!(PartInstance, wireframe_color) as wgpu::BufferAddress,
							shader_location: 2,
						},
					],
				}],
			},
			primitive: wgpu::PrimitiveState {
				topology: wgpu::PrimitiveTopology::TriangleList,
				strip_index_format: None,
				front_face: wgpu::FrontFace::Ccw,
				cull_mode: None,
				unclipped_depth: false,
				polygon_mode: wgpu::PolygonMode::Fill,
				conservative: false,
			},
			depth_stencil: Some(wgpu::DepthStencilState {
				format: display.depth_stencil_format.to_format(),
				depth_write_enabled: false,
				depth_compare: wgpu::CompareFunction::LessEqual,
				stencil: wgpu::StencilState::default(),
				bias: wgpu::DepthBiasState::default(),
			}),
			multisample: wgpu::MultisampleState::default(),
			fragment: Some(wgpu::FragmentState {
				module,
				entry_point: "fs_outline",
				targets: &targets,
			}),
			multiview: None,
		};

		let pipeline = device.create_render_pipeline(&pipeline_descriptor);

		Ok(Rc::new(PartWireframePipeline {
			pipeline,
			shared: common,
		}))
	}

	pub fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>, part_buffer: wgpu::BufferSlice<'r>, part_count: u32) {
		render_pass.set_pipeline(&self.pipeline);
		render_pass.set_bind_group(0, &self.shared.common.shared_shader_group, &[]);
		render_pass.set_index_buffer(self.shared.part_wire2_index(), wgpu::IndexFormat::Uint16);
		render_pass.set_vertex_buffer(0, part_buffer);
		render_pass.draw_indexed(0..PART_WIRE2_INDICES, 0, 0..part_count);
	}
}
