use crate::hud::{UIRect, UIColor, Corners};
use super::*;
use super::buffers::StreamingBuffer;

#[repr(C)]
pub struct UIPartRepr {
	pub rect: UIRect,
	pub color: UIColor,
	pub round_corners: Corners,
}

pub struct UIPartRenderingPipeline {
	pipeline: wgpu::RenderPipeline,
	param_buffer: wgpu::Buffer,
	ui_bind_group: wgpu::BindGroup,
}

#[repr(C)]
struct UIParams {
	viewport_inv: [f32; 2],
}

impl UIPartRenderingPipeline {
	pub(crate) fn new(display: &Display, shader_store: &ShaderStore) -> AnyResult<Self> {
		let device = &display.device;
		let shader = Self::load_shaders(device, shader_store)?;
		let module = &shader;

		let ui_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
			label: None,
			entries: &[
				wgpu::BindGroupLayoutEntry {
					binding: 0,
					visibility: wgpu::ShaderStages::VERTEX_FRAGMENT,
					ty: wgpu::BindingType::Buffer {
						ty: wgpu::BufferBindingType::Storage { read_only: true },
						has_dynamic_offset: false,
						min_binding_size: Some(buffer_size::<UIParams>()),
					},
					count: None,
				},
			],
		});

		let param_buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label: None,
			size: size_of::<UIParams>() as wgpu::BufferAddress,
			usage: wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
			mapped_at_creation: false,
		});

		let ui_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
			label: None,
			layout: &ui_group_layout,
			entries: &[wgpu::BindGroupEntry {
				binding: 0,
				resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
					buffer: &param_buffer,
					offset: 0,
					size: Some(buffer_size::<UIParams>()),
				}),
			}],
		});

		let pld: wgpu::PipelineLayoutDescriptor = wgpu::PipelineLayoutDescriptor {
			label: Some("UI Pipeline layout"),
			bind_group_layouts: &[&ui_group_layout],
			push_constant_ranges: &[],
		};
		let layout = device.create_pipeline_layout(&pld);

		let targets = [Some(wgpu::ColorTargetState {
			format: display.config.format,
			blend: Some(wgpu::BlendState::ALPHA_BLENDING),
			write_mask: wgpu::ColorWrites::ALL,
		})];

		let pipeline_descriptor = wgpu::RenderPipelineDescriptor {
			label: Some("ui-part pipeline"),
			layout: Some(&layout),
			vertex: wgpu::VertexState {
				module,
				entry_point: "vs_main",
				buffers: &[wgpu::VertexBufferLayout {
					array_stride: size_of::<UIPartRepr>() as wgpu::BufferAddress,
					step_mode: wgpu::VertexStepMode::Instance,
					attributes: &[
						// Position
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x2,
							offset: offset_of!(UIPartRepr, rect.x) as wgpu::BufferAddress,
							shader_location: 0,
						},
						// Extent
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x2,
							offset: offset_of!(UIPartRepr, rect.w) as wgpu::BufferAddress,
							shader_location: 1,
						},
						// Color
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Unorm8x4,
							offset: offset_of!(UIPartRepr, color) as wgpu::BufferAddress,
							shader_location: 2,
						},
						// Corner radii
						wgpu::VertexAttribute {
							format: wgpu::VertexFormat::Float32x4,
							offset: offset_of!(UIPartRepr, round_corners) as wgpu::BufferAddress,
							shader_location: 3,
						},
					],
				}],
			},
			primitive: wgpu::PrimitiveState {
				topology: wgpu::PrimitiveTopology::TriangleStrip,
				strip_index_format: None,
				front_face: wgpu::FrontFace::Ccw,
				// For debug
				cull_mode: Some(wgpu::Face::Back),
				unclipped_depth: false,
				polygon_mode: wgpu::PolygonMode::Fill,
				conservative: false,
			},
			depth_stencil: display.depth_stencil_state_disabled(),
			multisample: wgpu::MultisampleState::default(),
			fragment: Some(wgpu::FragmentState {
				module,
				entry_point: "fs_fill",
				targets: &targets,
			}),
			multiview: None,
		};

		let pipeline = device.create_render_pipeline(&pipeline_descriptor);

		Ok(Self {
			pipeline,
			param_buffer,
			ui_bind_group,
		})
	}

	fn load_shaders(device: &wgpu::Device, shader_store: &ShaderStore) -> AnyResult<wgpu::ShaderModule> {
		let src_part = shader_store.load_shader_src("ui-part")?;

		let mod_part = device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("ui-part shader"),
			source: wgpu::ShaderSource::Wgsl(std::borrow::Cow::Borrowed(src_part)),
		});

		Ok(mod_part)
	}

	pub(crate) fn render<'s: 'r, 'r>(&'s self, render_pass: &mut wgpu::RenderPass<'r>, fill_buffer: &'s StreamingBuffer, _stroke_buffer: &'s StreamingBuffer) {
		let parts_to_render = fill_buffer.size_elems::<UIPartRepr>() as u32;
		if parts_to_render == 0 {
			return;
		}
		render_pass.set_pipeline(&self.pipeline);
		render_pass.set_bind_group(0, &self.ui_bind_group, &[]);
		render_pass.set_vertex_buffer(0, fill_buffer.full_slice());
		render_pass.draw(0..4, 0..parts_to_render);
	}

	#[inline]
	pub(crate) fn update_viewport(&self, display: &Display) {
		let params = UIParams {
			viewport_inv: [
				1.0 / display.config.width as f32,
				1.0 / display.config.height as f32,
			],
		};
		display.queue.write_buffer(&self.param_buffer, 0, params.as_bytes());
	}
}
