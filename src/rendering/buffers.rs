use super::*;

use wgpu::BufferAddress as Baddr;

/// A buffer that behaves like a dynamic array, i.e. is resizable.
pub struct DynamicBuffer {
	buffer: wgpu::Buffer,
	size: Baddr,
}

impl DynamicBuffer {
	pub const REQUIRED_BUFFER_USAGES: wgpu::BufferUsages = {
		use wgpu::BufferUsages as U;
		U::MAP_READ.union(U::MAP_WRITE).union(U::COPY_DST)
	};

	pub fn new_empty(device: &wgpu::Device, usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size: 0,
			usage,
			mapped_at_creation: false,
		});

		DynamicBuffer {
			buffer,
			size: 0,
		}
	}

	pub fn with_capacity(device: &wgpu::Device, mut capacity: Baddr, usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		use wgpu::COPY_BUFFER_ALIGNMENT as ALIGN;
		const MASK: Baddr = ALIGN - 1;
		capacity = (capacity + MASK) & !MASK;
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size: capacity,
			usage,
			mapped_at_creation: false,
		});

		DynamicBuffer {
			buffer,
			size: 0,
		}
	}

	pub fn with_init(device: &wgpu::Device, bytes: &[u8], usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		let len = bytes.len() as Baddr;
		if len == 0 {
			return Self::new_empty(device, usage, label);
		}
		assert_eq!(len & (wgpu::MAP_ALIGNMENT - 1), 0);
		let size = growing::choose_capacity(len);
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size,
			usage,
			mapped_at_creation: true,
		});

		buffer.slice(..len).get_mapped_range_mut().copy_from_slice(bytes);
		buffer.unmap();

		DynamicBuffer {
			buffer,
			size: len,
		}
	}

	#[inline(always)]
	#[doc(alias = "len")]
	/// Currently used size.
	pub fn size(&self) -> Baddr {
		self.size
	}

	#[inline(always)]
	#[doc(alias = "len_elems")]
	/// Number of allocated elements of type `T`.
	pub fn size_elems<T: Sized>(&self) -> Baddr {
		self.size / size_of::<T>() as Baddr
	}

	#[inline(always)]
	pub fn capacity(&self) -> Baddr {
		self.buffer.size()
	}

	/// Note: when empty, this will return the slice at the end of the buffer,
	/// as it's the only way to have a zero-sized slice
	#[inline]
	pub fn full_slice(&self) -> wgpu::BufferSlice {
		if self.size == 0 {
			self.buffer.slice(self.buffer.size()..)
		} else {
			self.buffer.slice(..self.size)
		}
	}

	pub fn push(&mut self, device: &wgpu::Device, queue: &wgpu::Queue, bytes: &[u8]) {
		let min_size = self.size() + bytes.len() as Baddr;
		if min_size > self.capacity() {
			let new_size = growing::choose_capacity(min_size);
			// Reallocate
			let new_buffer = device.create_buffer(&wgpu::BufferDescriptor {
				// TODO
				//label: self.label,
				label: None,
				size: new_size,
				usage: self.buffer.usage(),
				mapped_at_creation: true,
			});

			self.buffer.slice(..).map_async(wgpu::MapMode::Read, |result| {
				result.unwrap();
			});

			while matches!(device.poll(wgpu::Maintain::Wait), wgpu::MaintainResult::Ok) {}

			{
				// TODO ODSTDST: Convert copying to CommandEncoder::copy_buffer_to_buffer
				let old = self.full_slice().get_mapped_range();
				let mut new = new_buffer.slice(0..min_size).get_mapped_range_mut();
				let offset = usize::try_from(self.size()).unwrap();
				// assert_eq!(offset as Baddr, self.buffer.size());
				new[..offset].copy_from_slice(&old);
				new[offset..].copy_from_slice(bytes);
			}

			self.buffer.unmap();
			new_buffer.unmap();

			self.buffer = new_buffer;
		} else {
			let offset = self.size();
			queue.write_buffer(&self.buffer, offset, bytes);
		}

		self.size += bytes.len() as Baddr;
	}

	#[inline]
	pub fn reset(&mut self) {
		self.size = 0;
	}
}

mod growing {
	use super::Baddr;
	pub const GRANULARITY: Baddr = 0x100;
	#[inline]
	pub fn choose_capacity(size: Baddr) -> Baddr {
		(size + (GRANULARITY - 1)) & !(GRANULARITY - 1)
	}
}

/// Like `DynamicBuffer`, but with more efficient uploading strategy.
pub struct UpdatableBuffer {
	buffer: wgpu::Buffer,
	size: Baddr,
	/// This bufffer is always mapped
	front_buffer: wgpu::Buffer,
	//// This buffer is unmapped
	//back_buffer: Option<wgpu::Buffer>,
	// memory: Vec<u8>,
	ranges: rangemap::RangeSet<Baddr>,
}

mod staging {
	use super::Baddr;
	pub const STAGING_BUFFER_USAGES: wgpu::BufferUsages = {
		use wgpu::BufferUsages as U;
		U::MAP_WRITE.union(U::COPY_SRC)
	};

	pub const DESTINATION_BUFFER_USAGES: wgpu::BufferUsages = {
		use wgpu::BufferUsages as U;
		U::COPY_DST.union(U::COPY_SRC)
	};

	pub const STAGING_BUFFER_LABEL: wgpu::Label<'static> = Some("Staging buffer");

	pub fn make_staging(device: &wgpu::Device, size: Baddr) -> wgpu::Buffer {
		device.create_buffer(&wgpu::BufferDescriptor {
			label: STAGING_BUFFER_LABEL,
			size,
			usage: STAGING_BUFFER_USAGES,
			mapped_at_creation: true,
		})
	}
}

impl UpdatableBuffer {
	pub const REQUIRED_BUFFER_USAGES: wgpu::BufferUsages = staging::DESTINATION_BUFFER_USAGES;

	pub fn new_empty(device: &wgpu::Device, usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size: 0,
			usage,
			mapped_at_creation: false,
		});

		let staging_buffer = staging::make_staging(device, 0);

		UpdatableBuffer {
			buffer,
			size: 0,
			front_buffer: staging_buffer,
			//back_buffer: None,
			ranges: rangemap::RangeSet::new(),
		}
	}

	pub fn with_capacity(device: &wgpu::Device, mut capacity: Baddr, usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		use wgpu::COPY_BUFFER_ALIGNMENT as ALIGN;
		const MASK: Baddr = ALIGN - 1;
		capacity = (capacity + MASK) & !MASK;
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size: capacity,
			usage,
			mapped_at_creation: false,
		});

		let staging_buffer = staging::make_staging(device, capacity);

		UpdatableBuffer {
			buffer,
			size: capacity,
			front_buffer: staging_buffer,
			//back_buffer: None,
			ranges: rangemap::RangeSet::new(),
		}
	}

	pub fn with_init(device: &wgpu::Device, cmd: &mut wgpu::CommandEncoder, bytes: &[u8], usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		let len = bytes.len() as Baddr;
		if len == 0 {
			return Self::new_empty(device, usage, label);
		}
		assert_eq!(len & (wgpu::MAP_ALIGNMENT - 1), 0);
		let size = growing::choose_capacity(len);
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size,
			usage,
			mapped_at_creation: false,
		});

		let staging_buffer = staging::make_staging(device, size);

		staging_buffer.slice(..len).get_mapped_range_mut().copy_from_slice(bytes);
		cmd.copy_buffer_to_buffer(&staging_buffer, 0, &buffer, 0, len);

		UpdatableBuffer {
			buffer,
			size,
			front_buffer: staging_buffer,
			//back_buffer: None,
			ranges: rangemap::RangeSet::new(),
		}
	}

	#[inline(always)]
	#[doc(alias = "len")]
	/// Currently used size.
	pub fn size(&self) -> Baddr {
		self.size
	}

	#[inline(always)]
	#[doc(alias = "len_elems")]
	/// Number of allocated elements of type `T`.
	pub fn size_elems<T: Sized>(&self) -> Baddr {
		self.size / size_of::<T>() as Baddr
	}

	#[inline(always)]
	pub fn capacity(&self) -> Baddr {
		self.buffer.size()
	}

	/// Note: when empty, this will return the slice at the end of the buffer,
	/// as it's the only way to have a zero-sized slice
	#[inline]
	pub fn full_slice(&self) -> wgpu::BufferSlice {
		if self.size == 0 {
			self.buffer.slice(self.buffer.size()..)
		} else {
			self.buffer.slice(..self.size)
		}
	}

	/// Append data at the end of the buffer.
	/// When flushing, the pending writes & buffer size should be
	/// aligned to [`wgpu::COPY_BUFFER_ALIGNMENT`] or bad things will happen.
	pub fn push(&mut self, device: &wgpu::Device, cmd: &mut wgpu::CommandEncoder, bytes: &[u8]) {
		let min_size = self.size() + bytes.len() as Baddr;
		if min_size > self.capacity() {
			let new_size = growing::choose_capacity(min_size);
			// Reallocate
			let new_buffer = device.create_buffer(&wgpu::BufferDescriptor {
				// TODO
				//label: self.label,
				label: None,
				size: new_size,
				usage: self.buffer.usage(),
				mapped_at_creation: false,
			});

			//let new_staging = staging::make_staging(device, new_size);

			cmd.copy_buffer_to_buffer(&self.buffer, 0, &new_buffer, 0, self.size());
			//self.front_buffer.unmap();

			self.flush_pending_internal(device, cmd, new_size);
			//println!("New buffer size: {}", self.front_buffer.size());

			//self.front_buffer = new_staging;
			self.buffer = new_buffer;
		}

		//println!("Write slice: {}..{}", self.size(), min_size);
		//self.front_buffer.slice(self.size()..min_size).get_mapped_range_mut().copy_from_slice(bytes);
		{
			const _: () = assert!(wgpu::MAP_ALIGNMENT.is_power_of_two());
			let map_start = self.size() & !(wgpu::MAP_ALIGNMENT - 1);
			let offset = (self.size() - map_start) as usize;
			self.front_buffer.slice(map_start..min_size).get_mapped_range_mut()[offset..].copy_from_slice(bytes);
		}

		self.ranges.insert(self.size()..min_size);
		self.size += bytes.len() as Baddr;
	}

	/// Overwrites parts of the buffer within current **size**.
	/// Restrictions around size apply as with [`push`][Self::push].
	pub fn write_inside(&mut self, offset: Baddr, data: &[u8]) {
		assert!(offset <= self.size(), "Offset out of bounds");
		let end_offset = offset + data.len() as Baddr;
		assert!(end_offset <= self.size(), "Data overflows the buffer");

		{
			const _: () = assert!(wgpu::MAP_ALIGNMENT.is_power_of_two());
			let map_start = offset & !(wgpu::MAP_ALIGNMENT - 1);
			let map_offset = (offset - map_start) as usize;
			self.front_buffer.slice(map_start..end_offset).get_mapped_range_mut()[map_offset..].copy_from_slice(data);
		}

		self.ranges.insert(offset..end_offset);
	}

	/// Truncate the buffer to have as most `new_size` bytes.
	/// Does nothing if the buffer stores less or equal bytes.
	/// Restrictions around size apply as with [`push`][Self::push].
	pub fn truncate(&mut self, new_size: Baddr) {
		if self.size() <= new_size {
			return;
		}

		self.ranges.remove(new_size..self.size());
		self.size = new_size;
	}

	/// Checks whether pending changes are waiting, thus needing a command buffer.
	#[inline]
	pub fn needs_flushing(&self) -> bool {
		!self.ranges.is_empty()
	}

	/// Flushes all pending changes. Call this before using the resulting buffer.
	pub fn flush_pending(&mut self, device: &wgpu::Device, cmd: &mut wgpu::CommandEncoder) {
		if self.ranges.is_empty() {
			return;
		}
		self.flush_pending_internal(device, cmd, self.buffer.size());
	}

	fn flush_pending_internal(&mut self, device: &wgpu::Device, cmd: &mut wgpu::CommandEncoder, new_size: Baddr) {
		self.front_buffer.unmap();
		for range in self.ranges.iter() {
			cmd.copy_buffer_to_buffer(&self.front_buffer, range.start, &self.buffer, range.start, range.end - range.start);
		}
		/*match self.back_buffer {
			Some(ref mut back_buffer) => {
				back_buffer.slice(..).map_async(wgpu::MapMode::Write, |_| {});
				std::mem::swap(&mut self.front_buffer, back_buffer);
			}
			None => {
				self.back_buffer = Some(std::mem::replace(&mut self.front_buffer, staging::make_staging(device, self.size())));
			}
		}*/
		// This is just too hard aaaaaaaaaaa
		self.front_buffer = staging::make_staging(device, new_size);
		//self.staging_buffer.slice(..).map_async(wgpu::MapMode::Write, |_| {});
		self.ranges.clear();
	}

	#[inline]
	pub fn reset(&mut self) {
		self.size = 0;
		self.ranges.clear();
	}
}

/// A buffer used for continuous dynamic reuploading, for example generated data every frame.
/// This buffer is growable and will generally stay at its peak capacity.
pub struct StreamingBuffer {
	buffer: wgpu::Buffer,
	label: Option<Box<str>>,
	size: Baddr,
	/// This bufffer is always mapped
	stream_buffer: wgpu::Buffer,
	is_mapped: bool,
}

#[cfg(any())]
enum BufferOrUsage {
	Buffer(wgpu::Buffer),
	Usage(wgpu::BufferUsages),
}

impl StreamingBuffer {
	pub const REQUIRED_BUFFER_USAGES: wgpu::BufferUsages = staging::DESTINATION_BUFFER_USAGES;

	pub fn new_empty(device: &wgpu::Device, usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size: 0,
			usage,
			mapped_at_creation: false,
		});

		let stream_buffer = staging::make_staging(device, 0);

		StreamingBuffer {
			//buffer: BufferOrUsage::Usage(usage),
			buffer,
			label: label.map(From::from),
			size: 0,
			stream_buffer,
			is_mapped: true,
		}
	}

	pub fn with_capacity(device: &wgpu::Device, mut capacity: Baddr, usage: wgpu::BufferUsages, label: wgpu::Label) -> Self {
		debug_assert!(usage.contains(Self::REQUIRED_BUFFER_USAGES), "Buffer must have required usage flags");
		use wgpu::COPY_BUFFER_ALIGNMENT as ALIGN;
		const MASK: Baddr = ALIGN - 1;
		capacity = (capacity + MASK) & !MASK;
		/*let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size: capacity,
			usage,
			mapped_at_creation: false,
		});*/
		let buffer = device.create_buffer(&wgpu::BufferDescriptor {
			label,
			size: 0,
			usage,
			mapped_at_creation: false,
		});

		let stream_buffer = staging::make_staging(device, capacity);

		StreamingBuffer {
			//buffer: BufferOrUsage::Usage(usage),
			buffer,
			label: label.map(From::from),
			size: capacity,
			stream_buffer,
			is_mapped: true,
		}
	}

	#[inline(always)]
	#[doc(alias = "len")]
	/// Currently used size.
	pub fn size(&self) -> Baddr {
		self.size
	}

	#[inline(always)]
	pub fn is_empty(&self) -> bool {
		self.size == 0
	}

	#[inline(always)]
	#[doc(alias = "len_elems")]
	/// Number of allocated elements of type `T`.
	pub fn size_elems<T: Sized>(&self) -> Baddr {
		self.size / size_of::<T>() as Baddr
	}

	#[inline(always)]
	pub fn capacity(&self) -> Baddr {
		self.stream_buffer.size()
	}

	/// Note: when empty, this will return the slice at the end of the buffer,
	/// as it's the only way to have a zero-sized slice
	#[inline]
	pub fn full_slice(&self) -> wgpu::BufferSlice {
		if self.size == 0 {
			self.buffer.slice(self.buffer.size()..)
		} else {
			self.buffer.slice(..self.size)
		}
	}

	pub fn push(&mut self, device: &wgpu::Device, bytes: &[u8]) {
		let min_size = self.size() + bytes.len() as Baddr;
		if min_size > self.capacity() {
			let new_size = growing::choose_capacity(min_size);
			// Reallocate
			/*let new_buffer = device.create_buffer(&wgpu::BufferDescriptor {
				// TODO
				//label: self.label,
				label: None,
				size: new_size,
				usage: self.buffer.usage(),
				mapped_at_creation: false,
			});*/

			let new_stream = staging::make_staging(device, new_size);
			if !self.is_empty() {
				let mut dst = new_stream.slice(..self.size()).get_mapped_range_mut();
				let src = self.stream_buffer.slice(..self.size()).get_mapped_range();
				dst.copy_from_slice(&*src);
			}

			self.stream_buffer = new_stream;
			//self.buffer = new_buffer;
		}

		{
			const _: () = assert!(wgpu::MAP_ALIGNMENT.is_power_of_two());
			let map_start = self.size() & !(wgpu::MAP_ALIGNMENT - 1);
			let offset = (self.size() - map_start) as usize;
			self.stream_buffer.slice(map_start..min_size).get_mapped_range_mut()[offset..].copy_from_slice(bytes);
		}

		self.size += bytes.len() as Baddr;
	}

	/// Resets a transfer currently in progress
	#[inline]
	pub fn reset(&mut self) {
		self.size = 0;
	}

	/// Prepares for a new memory transfer. Call this before doing any writing.
	/// A transfer is terminated with [`flush`].
	///
	/// [`flush`]: Self::flush
	pub fn begin_transfer(&mut self, device: &wgpu::Device) {
		self.size = 0;
		if self.is_mapped {
			return;
		}
		self.stream_buffer.slice(..).map_async(wgpu::MapMode::Write, |result| {
			result.unwrap();
		});
		while matches!(device.poll(wgpu::Maintain::Wait), wgpu::MaintainResult::Ok) {}
		self.is_mapped = true;
	}

	/// Flushes all pending changes. Call this before using the resulting buffer.
	//#[must_use]
	pub fn flush(&mut self, device: &wgpu::Device, cmd: &mut wgpu::CommandEncoder) {
		if self.is_empty() {
			return; //self.buffer.slice(self.buffer.size()..);
		}

		self.stream_buffer.unmap();
		self.is_mapped = false;

		/*let buffer = match self.buffer {
			BufferOrUsage::Usage(usage) => {
				self.buffer = BufferOrUsage::Buffer(device.create_buffer(&wgpu::BufferDescriptor {
					label: self.label.as_deref(),
					size: self.capacity(),
					usage,
					mapped_at_creation: false,
				}));
				match self.buffer {
					BufferOrUsage::Buffer(ref buf) => buf,
					_ => unsafe { std::hint::unreachable_unchecked() },
				}
			}
			BufferOrUsage::Buffer(ref buf) if buf.size() < self.size() => {
				// Reallocate anyway
				self.buffer = BufferOrUsage::Buffer(device.create_buffer(&wgpu::BufferDescriptor {
					label: self.label.as_deref(),
					size: self.capacity(),
					usage: buf.usage(),
					mapped_at_creation: false,
				}));
				match self.buffer {
					BufferOrUsage::Buffer(ref buf) => buf,
					_ => unsafe { std::hint::unreachable_unchecked() },
				}
			}
			BufferOrUsage::Buffer(ref buf) => buf,
		};*/
		if self.buffer.size() < self.size() {
			self.buffer = device.create_buffer(&wgpu::BufferDescriptor {
				label: self.label.as_deref(),
				size: self.capacity(),
				usage: self.buffer.usage(),
				mapped_at_creation: false,
			});
		}
		let buffer = &self.buffer;

		cmd.copy_buffer_to_buffer(&self.stream_buffer, 0, buffer, 0, self.size());
		/*if self.size == 0 {
			buffer.slice(buffer.size()..)
		} else {
			buffer.slice(..self.size)
		}*/
	}
}
