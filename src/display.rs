use crate::rendering::{DepthStencilFormat, TextureAndView, create_depth_texture};
use winit::dpi::PhysicalSize;

pub(crate) struct Display {
	//pub event_loop: EventLoop<()>,
	pub win: winit::window::Window,
	pub instance: wgpu::Instance,
	pub surface: wgpu::Surface<'static>,
	pub adapter: wgpu::Adapter,
	pub device: wgpu::Device,
	pub queue: wgpu::Queue,
	//pub format: wgpu::TextureFormat,
	//pub alpha_mode: wgpu::CompositeAlphaMode,
	pub config: wgpu::SurfaceConfiguration,
	pub depth_stencil_format: DepthStencilFormat,
	pub depth_stencil_texture: Option<TextureAndView>,
}

/// This is a hack
pub struct DisplayRef(&'static Display);

impl DisplayRef {
	#[inline(always)] pub fn win(&self) -> &winit::window::Window { &self.0.win }
	#[inline(always)] pub fn instance(&self) -> &wgpu::Instance { &self.0.instance }
	#[inline(always)] pub fn surface(&self) -> &wgpu::Surface { &self.0.surface }
	#[inline(always)] pub fn adapter(&self) -> &wgpu::Adapter { &self.0.adapter }
	#[inline(always)] pub fn device(&self) -> &wgpu::Device { &self.0.device }
	#[inline(always)] pub fn queue(&self) -> &wgpu::Queue { &self.0.queue }

	#[inline(always)] pub fn surface_format(&self) -> wgpu::TextureFormat { self.0.config.format }
	#[inline(always)] pub fn depth_stencil_format(&self) -> DepthStencilFormat { self.0.depth_stencil_format }
	#[inline(always)] pub fn dimensions(&self) -> [u32; 2] { [self.0.config.width, self.0.config.height] }
}

impl Display {
	pub fn resize(&mut self, new_size: PhysicalSize<u32>) {
		let PhysicalSize { width: 1.., height: 1.. } = new_size else {
			return;
		};
		self.config.width = new_size.width;
		self.config.height = new_size.height;
		self.surface.configure(&self.device, &self.config);
		if let Some(ref mut depth_stencil) = self.depth_stencil_texture {
			// Recreate depth stencil texture
			*depth_stencil = create_depth_texture(&self.device, &self.config, self.depth_stencil_format);
		}
	}

	pub fn configure(&mut self) {
		self.surface.configure(&self.device, &self.config);
	}

	#[inline(always)]
	pub unsafe fn make_ref(&self) -> DisplayRef {
		DisplayRef(unsafe { crate::util::reborrow(self) })
	}

	pub fn depth_stencil_state_disabled(&self) -> Option<wgpu::DepthStencilState> {
		if self.depth_stencil_texture.is_some() {
			Some(wgpu::DepthStencilState {
				format: self.depth_stencil_format.to_format(),
				depth_write_enabled: false,
				depth_compare: wgpu::CompareFunction::Always,
				stencil: wgpu::StencilState::default(),
				bias: wgpu::DepthBiasState::default(),
			})
		} else {
			None
		}
	}
}
