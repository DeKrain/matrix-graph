use std::cell::RefCell;

use glyphon::cosmic_text as ct;
use crate::Display;
use color_eyre::eyre::Result as AnyResult;

use ct::{Attrs, AttrsList, AttrsOwned, Buffer, BufferLine};

pub struct TextState {
	pub font_system: RefCell<ct::FontSystem>,
	swash_cache: ct::SwashCache,
	font_atlas: glyphon::TextAtlas,
	text_renderer: glyphon::TextRenderer,

	text_areas: Vec<glyphon::TextArea<'static>>,
}

impl TextState {
	pub fn new(device: &wgpu::Device, queue: &wgpu::Queue, format: wgpu::TextureFormat, depth_stencil: Option<wgpu::DepthStencilState>) -> Self {
		let font_system = ct::FontSystem::new();
		let mut font_atlas = glyphon::TextAtlas::new(&device, &queue, format);
		let text_renderer = glyphon::TextRenderer::new(&mut font_atlas, &device, wgpu::MultisampleState::default(), depth_stencil);
		let swash_cache = ct::SwashCache::new();
		// font_renderer.prepare(&device, &queue, &mut font_system, &mut font_atlas, screen_resolution, text_areas, &mut swash_cache);

		TextState {
			font_system: RefCell::new(font_system),
			swash_cache,
			font_atlas,
			text_renderer,

			text_areas: Vec::new(),
		}
	}
}

pub struct TextFrame<'state> {
	state: &'state mut TextState,
}

impl std::ops::Deref for TextFrame<'_> {
	type Target = TextState;
	#[inline(always)] fn deref(&self) -> &Self::Target { self.state }
}

impl std::ops::DerefMut for TextFrame<'_> {
	#[inline(always)] fn deref_mut(&mut self) -> &mut Self::Target { self.state }
}

impl<'state> TextFrame<'state> {
	pub(crate) fn prepare(self, display: &Display) -> AnyResult<()> {
		let state = self.state;
		state.text_renderer.prepare(
			&display.device,
			&display.queue,
			state.font_system.get_mut(),
			&mut state.font_atlas,
			glyphon::Resolution { width: display.config.width, height: display.config.height },
			state.text_areas.drain(..),
			&mut state.swash_cache,
		)?;
		Ok(())
	}

	#[inline(always)]
	pub fn push_area<'f>(&'f mut self, area: glyphon::TextArea<'f>) {
		// We only allow areas constrained to the current frame so we can reuse the area buffer
		self.text_areas.push(unsafe { std::mem::transmute(area) });
	}
}

impl TextState {
	#[inline(always)]
	pub fn new_frame(&mut self) -> TextFrame {
		// We must ensure no areas were left while creating the frame.
		// The are buffer is only used inside TextFrame, so this should be safe.
		self.text_areas.clear();
		TextFrame { state: self }
	}

	#[inline]
	pub fn render<'s: 'pass, 'pass>(&'s self, pass: &mut wgpu::RenderPass<'pass>) -> AnyResult<()> {
		self.text_renderer.render(&self.font_atlas, pass)?;
		Ok(())
	}

	#[inline]
	pub fn create_buffer(&self, font_size: f32, line_height: f32) -> ct::Buffer {
		let mut buffer = ct::Buffer::new_empty(ct::Metrics { font_size, line_height });
		buffer.set_size(&mut self.font_system.borrow_mut(), f32::MAX, f32::MAX);
		buffer
	}

	#[inline]
	pub fn create_buffer_width(&self, font_size: f32, line_height: f32, width: f32) -> ct::Buffer {
		let mut buffer = ct::Buffer::new_empty(ct::Metrics { font_size, line_height });
		buffer.set_size(&mut self.font_system.borrow_mut(), width, f32::MAX);
		buffer
	}

	#[inline]
	pub fn create_buffer_line(&self, font_size: f32, line_height: f32, line: &TextLine) -> ct::Buffer {
		let mut buffer = ct::Buffer::new_empty(ct::Metrics { font_size, line_height });
		let mut font_system = self.font_system.borrow_mut();
		buffer.set_size(&mut *font_system, f32::MAX, f32::MAX);
		buffer.lines.push(line.buffer_line());
		buffer.shape_until_scroll(&mut *font_system);
		buffer
	}

	pub fn buffer_width(buffer: &Buffer) -> f32 {
		let mut width = 0.0;
		for line in &buffer.lines {
			for line in line.layout_opt().as_ref().unwrap() {
				if line.w > width {
					width = line.w;
				}
			}
		}
		width
	}
}


/// A clonable version of [`ct::AttrsList`]
#[derive(Clone)]
pub struct TextAttrsList {
	pub defaults: AttrsOwned,
	pub spans: rangemap::RangeMap<usize, AttrsOwned>,
}

impl TextAttrsList {
	#[inline]
	pub fn new<A: for<'a> Into<Attrs<'a>>>(attrs: A) -> Self {
		TextAttrsList {
			defaults: AttrsOwned::new(attrs.into()),
			spans: rangemap::RangeMap::new(),
		}
	}

	pub fn to_attrs_list(&self) -> AttrsList {
		let mut list = AttrsList::new(self.defaults.as_attrs());
		for (range, attrs) in self.spans.iter() {
			list.add_span(range.clone(), attrs.as_attrs());
		}
		list
	}
}

#[derive(Clone)]
pub struct TextLine {
	pub text: String,
	pub attrs: TextAttrsList,
}

impl TextLine {
	#[inline]
	pub fn new<T: Into<String>, A: for<'a> Into<Attrs<'a>>>(str: T, attrs: A) -> Self {
		TextLine {
			text: str.into(),
			attrs: TextAttrsList::new(attrs),
		}
	}

	pub fn buffer_line(&self) -> ct::BufferLine {
		make_buffer_line(&self.text, self.attrs.to_attrs_list(), ct::Shaping::Advanced)
	}

	pub fn build_buffer_line(&self) -> BufferLineBuilder {
		BufferLineBuilder(make_buffer_line(&self.text, self.attrs.to_attrs_list(), ct::Shaping::Advanced))
	}
}

#[inline]
fn make_buffer_line<T: Into<String>>(text: T, attrs_list: AttrsList, shaping: ct::Shaping) -> BufferLine {
	let mut line = BufferLine::new(text, attrs_list, shaping);
	line.set_wrap(ct::Wrap::None);
	line
}

#[repr(transparent)]
pub struct BufferLineBuilder(pub BufferLine);

impl std::ops::Deref for BufferLineBuilder {
	type Target = BufferLine;
	#[inline(always)] fn deref(&self) -> &Self::Target { &self.0 }
}
impl std::ops::DerefMut for BufferLineBuilder {
	#[inline(always)] fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

impl BufferLineBuilder {
	#[inline(always)] pub fn wrap(mut self, wrap: ct::Wrap) -> Self {
		self.0.set_wrap(wrap);
		self
	}
	#[inline(always)] pub fn align(mut self, align: ct::Align) -> Self {
		self.0.set_align(Some(align));
		self
	}
	#[inline(always)] pub fn no_align(mut self) -> Self {
		self.0.set_align(None);
		self
	}
}
