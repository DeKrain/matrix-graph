use std::future::{Future, IntoFuture};
use std::sync::Arc;
use std::task::{Wake, Context, Poll};

struct ThreadParker {
	thread: std::thread::Thread,
}

impl ThreadParker {
	#[inline]
	fn new() -> Self {
		ThreadParker {
			thread: std::thread::current(),
		}
	}
}

impl Wake for ThreadParker {
	#[inline]
	fn wake(self: Arc<Self>) {
		self.wake_by_ref()
	}

	#[inline]
	fn wake_by_ref(self: &Arc<Self>) {
		self.thread.unpark()
	}
}

pub fn block_on<F>(future: F) -> F::Output
where
	F: IntoFuture
{
	let mut future = std::pin::pin!(future.into_future());
	let waker = Arc::new(ThreadParker::new()).into();
	let mut cx = Context::from_waker(&waker);
	loop {
		match future.as_mut().poll(&mut cx) {
			Poll::Ready(out) => return out,
			Poll::Pending => std::thread::park(),
		}
	}
}
