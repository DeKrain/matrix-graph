use crate::{
	rendering::{
		buffers::UpdatableBuffer,
		PartPipeline,
	},
	util::SliceExt,
};

#[repr(C)]
pub struct PartInstance {
	pub origin: [f32; 3],
	pub extent: [f32; 3],
	pub wireframe_color: [u8; 4],
}

/// A container for Parts. There is usually only one [`PartPipeline`], but
/// multiple [`PartContainer`]s may use it.
pub struct PartContainer {
	part_buffer: UpdatableBuffer,
	buffer_encoder: Option<wgpu::CommandEncoder>,
}

impl PartContainer {
	pub fn new(device: &wgpu::Device) -> Self {
		let part_buffer = UpdatableBuffer::new_empty(device, PartPipeline::VERTEX_BUFFER_USAGES, Some("Part container"));

		PartContainer {
			part_buffer,
			buffer_encoder: None,
		}
	}

	#[inline]
	pub fn add_part(&mut self, device: &wgpu::Device, part: PartInstance) {
		self.part_buffer.push(device, Self::ensure_buffer_encoder(&mut self.buffer_encoder, device), part.as_bytes());
	}

	pub fn replace_part(&mut self, index: u32, replacement: PartInstance) {
		self.part_buffer.write_inside(index as wgpu::BufferAddress * std::mem::size_of::<PartInstance>() as wgpu::BufferAddress, replacement.as_bytes());
	}

	pub fn truncate(&mut self, count: u32) {
		self.part_buffer.truncate(count as wgpu::BufferAddress * std::mem::size_of::<PartInstance>() as wgpu::BufferAddress);
	}

	#[inline]
	pub fn part_count(&self) -> u32 {
		self.part_buffer.size_elems::<PartInstance>().try_into().unwrap()
	}

	#[inline]
	pub fn full_slice(&self) -> wgpu::BufferSlice {
		self.part_buffer.full_slice()
	}

	#[inline]
	pub fn clear(&mut self) {
		self.part_buffer.reset();
	}

	#[must_use]
	pub fn flush_updates(&mut self, device: &wgpu::Device) -> Option<wgpu::CommandBuffer> {
		if self.part_buffer.needs_flushing() {
			self.part_buffer.flush_pending(device, Self::ensure_buffer_encoder(&mut self.buffer_encoder, device));
			let buffer = self.buffer_encoder.take().unwrap().finish();
			Some(buffer)
		} else {
			None
		}
	}

	fn ensure_buffer_encoder<'e>(encoder: &'e mut Option<wgpu::CommandEncoder>, device: &wgpu::Device) -> &'e mut wgpu::CommandEncoder {
		encoder.get_or_insert_with(
			|| device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
				label: None,
			})
		)
	}
}
