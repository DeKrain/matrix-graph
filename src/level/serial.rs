//! Serialization/deserialization of levels

use std::io::{Write, Read};

use super::*;
use crate::util::consts;
use serde::{Serialize, Deserialize, ser::SerializeSeq};
use color_eyre::eyre::eyre;

bitflags::bitflags! {
	#[derive(Clone, Copy)]
	pub struct SerializationFlags: u8 {
		const IncludeCameraRotation = 1 << 0;
	}
}

impl SerializationFlags {
	pub const EMPTY: Self = Self::empty();
}

const LEVEL_SERIAL_VERSION: u32 = 1;

pub struct SerializationOptions<'w> {
	pub writer: &'w mut dyn Write,
	pub flags: SerializationFlags,
}

pub struct DeserializationOptions<'r> {
	pub reader: &'r mut dyn Read,
}

type Vector3 = [f32; 3];

#[derive(Serialize)]
struct LevelSerial<'l> {
	version: u32,
	player: PlayerSerial,
	#[serde(skip_serializing_if = "Option::is_none")]
	camera: Option<CameraSerial>,
	parts: PartsSerial<'l>,
}

#[derive(Deserialize)]
struct LevelDeserial {
	#[allow(dead_code)]
	version: u32,
	player: PlayerSerial,
	camera: Option<CameraSerial>,
	parts: Vec<PartSerial>,
}

#[derive(Serialize, Deserialize)]
struct PlayerSerial {
	position: Vector3,
	velocity: Vector3,
	state: PlayerState,
	// ground_state?
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
enum PlayerState {
	Normal,
	Flying,
}

#[derive(Serialize, Deserialize)]
struct CameraSerial {
	/// Clamped to (-τ/2 ..= τ/2)
	yaw: f32,
	/// As on runtime, clamped to (-τ/4 .. τ/4)
	pitch: f32,
}

struct PartsSerial<'l> {
	parts: &'l [Box<Part>],
}

impl Serialize for PartsSerial<'_> {
	fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		let mut ser = serializer.serialize_seq(Some(self.parts.len()))?;
		for part in self.parts {
			let part = PartSerial {
				origin: part.origin.into(),
				extent: part.extent.into(),
				interaction: match part.interaction {
					PartInteraction::Static => InteractionSerial::Static,
					PartInteraction::Program(_) => InteractionSerial::Program,
				},
			};
			ser.serialize_element(&part)?;
		}
		ser.end()
	}
}

#[derive(Serialize, Deserialize)]
struct PartSerial {
	origin: Vector3,
	extent: Vector3,
	// color
	// opacity
	interaction: InteractionSerial,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
enum InteractionSerial {
	Static,
	Program,
}

impl LevelStage {
	pub fn serialize(&self, options: SerializationOptions) -> serde_json::Result<()> {
		let level = LevelSerial {
			version: LEVEL_SERIAL_VERSION,
			player: PlayerSerial {
				position: self.player.position.into(),
				velocity: self.player.velocity.into(),
				state: match self.player.ground_state {
					GroundState::FreeFlight => PlayerState::Flying,
					_ => PlayerState::Normal,
				},
			},
			camera: if options.flags.contains(SerializationFlags::IncludeCameraRotation) {
				let yaw = self.camera.yaw.rem_euclid(consts::TAU);
				Some(CameraSerial {
					yaw: if yaw > consts::FRAC_TAU_2 { yaw - consts::TAU } else { yaw },
					pitch: self.camera.pitch,
				})
			} else {
				None
			},
			parts: PartsSerial { parts: &self.parts },
		};

		serde_json::to_writer_pretty(options.writer, &level)
	}

	pub fn deserialize(&mut self, options: DeserializationOptions) -> AnyResult<()> {
		let level_raw: serde_json::Value = serde_json::from_reader(options.reader)?;
		if let Some(serde_json::Value::Number(verison)) = level_raw.get("version") {
			if let Some(version) = verison.as_u64().and_then(|n| u32::try_from(n).ok()) {
				if version != LEVEL_SERIAL_VERSION {
					eprintln!("Warning: level serial version doesn't match. Breaks may happen");
				}
			} else {
				return Err(eyre!("Version must be a valid uint32"));
			}
		} else {
			return Err(eyre!("Missing version field in level"));
		}
		let level: LevelDeserial = serde_json::from_value(level_raw)?;

		self.player.position = level.player.position.into();
		self.player.velocity = level.player.velocity.into();
		self.player.ground_state = match level.player.state {
			PlayerState::Normal => GroundState::Airbone,
			PlayerState::Flying => GroundState::FreeFlight,
		};
		if let Some(camera) = level.camera {
			self.camera.yaw = match camera.yaw.rem_euclid(consts::TAU) {
				yaw if yaw > consts::FRAC_TAU_2 => yaw - consts::TAU,
				yaw => yaw,
			};
			self.camera.pitch = camera.pitch.clamp((-consts::FRAC_TAU_4).next_up(), consts::FRAC_TAU_4.next_down());
		} else {
			// Restore defaults
			self.camera.yaw = 0.0;
			self.camera.pitch = 0.0;
		}
		self.clear_parts();
		self.parts.reserve(level.parts.len());
		for part in level.parts {
			// TODO: Add more part types
			self.add_cube(part.origin.into(), part.extent.into());
		}

		Ok(())
	}
}
