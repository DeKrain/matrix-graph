pub trait SliceExt {
	fn as_bytes(&self) -> &[u8];
}

/*impl<T> SliceExt for [T] {
	fn as_bytes(&self) -> &[u8] {
		let (l, m, r) = unsafe { self.align_to() };
		assert!(l.is_empty() && r.is_empty());
		m
	}
}*/

impl<T: ?Sized> SliceExt for T {
	#[inline]
	fn as_bytes(&self) -> &[u8] {
		unsafe {
			std::slice::from_raw_parts(self as *const T as *const u8, std::mem::size_of_val(self))
		}
	}
}

pub trait FloatMidPoint: Copy {
	fn midpoint(a: Self, b: Self) -> Self;
}

impl FloatMidPoint for f32 {
	#[inline(always)]
	fn midpoint(a: Self, b: Self) -> Self {
		a * 0.5 + b * 0.5
	}
}

impl FloatMidPoint for f64 {
	#[inline(always)]
	fn midpoint(a: Self, b: Self) -> Self {
		a * 0.5 + b * 0.5
	}
}

#[inline]
pub fn matrix_to_wgsl<S: FloatMidPoint>(matrix: &mut cgmath::Matrix4<S>) {
	matrix.x.z = S::midpoint(matrix.x.z, matrix.x.w);
	matrix.y.z = S::midpoint(matrix.y.z, matrix.y.w);
	matrix.z.z = S::midpoint(matrix.z.z, matrix.z.w);
	matrix.w.z = S::midpoint(matrix.w.z, matrix.w.w);
}

pub mod consts {
	pub use std::f32::consts::{
		*,
		PI as FRAC_TAU_2,
		FRAC_PI_2 as FRAC_TAU_4,
		FRAC_PI_3 as FRAC_TAU_6,
		FRAC_PI_4 as FRAC_TAU_8,
		FRAC_PI_6 as FRAC_TAU_12,
		FRAC_PI_8 as FRAC_TAU_16,
		FRAC_1_PI as FRAC_2_TAU,
	};
}

/// Changes the lifetime of a reference to overcome borrow checking limitations.
/// All lifetime requirements must be upheld by the user.
#[inline(always)]
pub unsafe fn reborrow<'inp, 'out, T>(refer: &'inp T) -> &'out T {
	&*(refer as *const T)
}

/// Changes the lifetime of a mutable reference to overcome borrow checking limitations.
/// All lifetime requirements must be upheld by the user.
#[inline(always)]
pub unsafe fn reborrow_mut<'inp, 'out, T>(refer: &'inp mut T) -> &'out mut T {
	&mut *(refer as *mut T)
}


#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Lerp<T>(T);

impl<T: Copy + std::ops::Add<Output = T> + std::ops::Sub<Output = T> + std::ops::Mul<Output = T>> Lerp<T> {
	#[inline]
	pub fn lerp(self, from: T, to: T) -> T {
		from + self.0 * (to - from)
	}
}

pub trait Delerp {
	type Frac;

	fn delerp(self, from: Self, to: Self) -> Lerp<Self::Frac>;
}

macro_rules! impl_delerp {
	(float $t:ty) => {
		impl Delerp for $t {
			type Frac = Self;

			#[inline]
			fn delerp(self, from: Self, to: Self) -> Lerp<Self> {
				Lerp((self - from) / (to - from))
			}
		}
	};
	
	(int $t:ty => $f:ty) => {
		impl Delerp for $t {
			type Frac = $f;

			#[inline]
			fn delerp(self, from: Self, to: Self) -> Lerp<$f> {
				Lerp((self - from) as $f / (to - from) as $f)
			}
		}
	};
}

impl_delerp!(float f32);
impl_delerp!(float f64);

impl_delerp!(int i64 => f64);
impl_delerp!(int u64 => f64);
impl_delerp!(int u32 => f32);
impl_delerp!(int i32 => f32);
impl_delerp!(int u16 => f32);
impl_delerp!(int i16 => f32);
impl_delerp!(int u8 => f32);
impl_delerp!(int i8 => f32);
