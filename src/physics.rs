use crate::level::*;

pub struct CollisionResult;

#[derive(Clone, Copy, PartialEq)]
pub struct RayIntersection {
	pub radius: f32,
}

impl Part {
	/// Retuns (lower, upper), where:
	/// coordinatewise, lower.xyz <= upper.xyz
	#[inline]
	pub(crate) fn lower_upper_bounds(&self) -> (Position, Position) {
		let extended = self.origin + self.extent;
		(
			self.origin.zip(extended, |a, b| a.min(b)),
			self.origin.zip(extended, |a, b| a.max(b)),
		)
	}

	#[inline]
	pub fn contains_point(&self, point: Position) -> bool {
		let (lower, upper) = self.lower_upper_bounds();
		let [lower, upper, point]: [&[f32; 3]; 3] = [
			lower.as_ref(), upper.as_ref(), point.as_ref()
		];
		std::iter::zip(lower, upper)
			.zip(point)
			.all(|((&l, &u), &p)| l <= p && p <= u)
	}

	// This doesn't necessarily count as physics, but
	// this is a fine place to put it.
	pub fn intersects_ray(&self, origin: Position, direction: Vector) -> Option<RayIntersection> {
		let (lower, upper) = self.lower_upper_bounds();

		// Find t such that:
		// > (org + t*dir).xyz in (lower.xyz ..= upper.xyz)
		// We can calculate bounds for t for each coordinate, then intersect them to find a minumum.
		struct MinMax<T> {
			min: T,
			max: T,
		}
		struct Coords<T> {
			x: T,
			y: T,
			z: T,
		}

		fn find_min_max(origin: f32, direction: f32, lower: f32, upper: f32) -> Option<MinMax<f32>> {
			if origin >= lower && origin <= upper {
				Some(MinMax {
					min: 0.0,
					max: if direction > 0.0 {
						(upper - origin) / direction
					} else if direction < 0.0 {
						(lower - origin) / direction
					} else {
						f32::INFINITY
					}
				})
			} else {
				if direction > 0.0 && origin < lower {
					let inv_dir = 1.0 / direction;
					Some(MinMax { min: (lower - origin) * inv_dir, max: (upper - origin) * inv_dir })
				} else if direction < 0.0 && origin > upper {
					let inv_dir = 1.0 / direction;
					Some(MinMax { min: (upper - origin) * inv_dir, max: (lower - origin) * inv_dir })
				} else {
					None
				}
			}
		}

		let coords = Coords {
			x: find_min_max(origin.x, direction.x, lower.x, upper.x),
			y: find_min_max(origin.y, direction.y, lower.y, upper.y),
			z: find_min_max(origin.z, direction.z, lower.z, upper.z),
		};

		let Coords {
			x: Some(xrange),
			y: Some(yrange),
			z: Some(zrange),
		} = coords else {
			return None;
		};

		let min = xrange.min.max(yrange.min).max(zrange.min);
		let max = xrange.max.min(yrange.max).min(zrange.max);

		// This doesn't return an intersection if there's only one common point.
		if min < max {
			Some(RayIntersection { radius: min })
		} else {
			None
		}
	}
}

impl Player {
	#[inline]
	pub fn collides_part(&self, part: &Part) -> Option<CollisionResult> {
		let lower = self.position + Vector { x: -self.hitbox_half_width, y: 0.0, z: -self.hitbox_half_width };
		let upper = self.position + Vector { x: self.hitbox_half_width, y: self.hitbox_height, z: self.hitbox_half_width };
		if part.contains_point(lower) || part.contains_point(upper) {
			Some(CollisionResult)
		} else {
			None
		}
	}
}
