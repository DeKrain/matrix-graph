use std::{cell::UnsafeCell, collections::HashMap};
use crate::AnyResult;

// This is Send, but not Sync (not re-entrant)
pub struct ShaderStore {
	shader_map: UnsafeCell<HashMap<&'static str, Box<str>>>,
}

impl ShaderStore {
	#[inline]
	pub fn new() -> Self {
		ShaderStore {
			shader_map: UnsafeCell::new(HashMap::new()),
		}
	}

	pub fn load_shader_src(&self, id: &'static str) -> AnyResult<&str> {
		use std::collections::hash_map::Entry;
		let entry = unsafe {
			// SAFETY: this function is not re-entrant, we are only adding entries.
			// Strings are boxed, so they won't be moved.
			(*self.shader_map.get()).entry(id)
		};
		Ok(match entry {
			Entry::Occupied(source) => source.into_mut(),
			Entry::Vacant(vac) => {
				let path = format!("shaders/{id}.wgsl");
				let source = std::fs::read_to_string(&path)
					.map_err(|err| color_eyre::eyre::Report::new(err).wrap_err(
						format!("Opening file {path}")))?;
				vac.insert(source.into_boxed_str())
			}
		})
	}
}
